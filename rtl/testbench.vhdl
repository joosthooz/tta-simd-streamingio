library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

entity testbench is end entity testbench;

architecture tb of testbench is
    constant stall_threshold : real := 0.1;
    signal out_data, in_data : std_logic_vector(32-1 downto 0);
    signal out_cnt, in_cnt : std_logic_vector(1 downto 0);
    signal out_valid, in_valid, in_ready, out_ready : std_logic;
    signal out_dvalid : std_logic;
    signal clk, rstx, reset, out_last : std_logic;
    signal write_idx : integer;
    signal in_last : std_logic;
    signal stage, cc : integer;
    signal first_read, final_done : std_logic;
    type character_file is file of character;
begin
    clk_gen : process
    begin
        rstx <= '0';
        clk <= '0';
        for i in 0 to 3 loop
          wait for 5 ns;
        end loop;
        rstx <= '1';
        while final_done /= '1' loop
            wait for 5 ns;
            clk <= not clk;
        end loop;
        report "Cycle count: " & integer'image(cc);
        wait;
    end process;

    cycle_counter: process(clk)
    begin
        if rstx = '0' then
          cc <= 0;
        elsif rising_edge(clk) then
          cc <= cc + 1;
          if out_ready = '1' and out_valid = '1' and out_last = '1' then
            cc <= 0;
          end if;
        end if;
    end process;

    stream_in: process(clk, rstx)
        FILE input : character_file; 
        variable data : character;
        variable fstatus : file_open_status;
        variable seed1 : integer := 56;
        variable seed2 : integer := 90;
        variable rand : real;
        variable cnt : integer;
    begin
        if rstx = '0' then
            first_read <= '0';
            in_valid <= '0';
            in_data <= (others => '0');
            in_last  <= '0';
            file_open(fstatus, input, "../data.compressed3", READ_MODE);
        elsif rising_edge(clk) then
            uniform(seed1, seed2, rand);

            if in_ready = '1' and in_valid = '1' then
                in_valid <= '0';
            end if;

            if (first_read = '0' or in_ready = '1' or in_valid = '0') and
               not endfile(input) and rand > stall_threshold then
                cnt := 0;
                for i in 0 to 3 loop
                    if not endfile(input) then
                        read(input, data);
                        in_data(i*8+7 downto i*8)
                            <= std_logic_vector(to_unsigned(character'pos(data), 8));
                        cnt := cnt + 1;
                    end if;
                end loop;
                if cnt = 4 then
                    cnt := 0;
                end if;
                in_cnt <= std_logic_vector(to_unsigned(cnt, 2));
                in_valid <= '1';
                first_read <= '1';

                in_last <= '0';
                if endfile(input) then
                    in_last <= '1';
                end if;
            end if;

            if out_ready = '1' and out_valid = '1' and out_last = '1' then
                file_close(input);
                case stage is
                    when 0 =>
                        file_open(fstatus, input, "../data.compressed2", READ_MODE);
                    when others =>
                        file_open(fstatus, input, "../data.compressed", READ_MODE);
                end case;
            end if;
        end if;
    end process;

    stream_out : process(clk, rstx)
        FILE output : character_file;
        variable data : character;
        variable fstatus : file_open_status;

        FILE golden : character_file;
        variable golden_data : character;
        
        variable seed1 : integer := 55;
        variable seed2 : integer := 91;
        variable rand : real;

        variable count : integer;
    begin
        if rstx = '0' then
            stage <= 0;
            final_done <= '0';
            write_idx <= 0;
            out_ready <= '0';

            file_open(fstatus, golden, "../data3.small.decompressed", READ_MODE);
            file_open(fstatus, output, "../third.out", WRITE_MODE); 
        elsif rising_edge(clk) then
            uniform(seed1, seed2, rand);
            if rand < stall_threshold then
               out_ready <= '0';
            else
               out_ready <= '1';
            end if;

            if out_cnt = "00" then
                count := 4;
            else
                count := to_integer(unsigned(out_cnt));
            end if;
            if out_ready = '1' and out_valid = '1' and out_dvalid = '1' then
                for i in 0 to count-1 loop
                    data := character'val(to_integer(unsigned(out_data(i*8+7 downto i*8))));
                    write(output, data);

                    read(golden, golden_data);
                    assert data = golden_data
                           report "Incorrect output, expected "
                                  & integer'image(character'pos(golden_data)) & ", got "
                              & integer'image(character'pos(data))
                           severity failure;
                    write_idx <= write_idx + 1;
                end loop;
            end if;

            if out_ready = '1' and out_valid = '1' and out_last = '1' then
                file_close(output);
                file_close(golden);
                case stage is
                    when 0 =>
                        report "Partial CC: " & integer'image(cc);
                        file_open(fstatus, golden, "../data2.small.decompressed", READ_MODE);
                        file_open(fstatus, output, "../second.out", WRITE_MODE);
                    when 1 =>
                        file_open(fstatus, golden, "../data.decompressed", READ_MODE);
                        file_open(fstatus, output, "../first.out", WRITE_MODE);
                        report "Partial CC: " & integer'image(cc);
                    when 2 =>
                        final_done <= '1';
                    when others =>
                end case;
                stage <= stage + 1;
            end if;
        end if;

    end process;

    reset <= not rstx;
    tta : entity work.tta_wrapper
    port map (
       clk => clk, reset => reset,
       
       in_valid => in_valid,
       in_ready => in_ready,
       in_data  => in_data,
       in_cnt   => in_cnt,
       in_last  => in_last,

       out_valid => out_valid,
       out_ready => out_ready,
       out_dvalid => out_dvalid,
       out_data => out_data,
       out_cnt => out_cnt,
       out_last => out_last
    );

end architecture;
