// This is a generated file. Use and modify at your own risk.
////////////////////////////////////////////////////////////////////////////////

/*******************************************************************************
Vendor: Xilinx
Associated Filename: main.c
#Purpose: This example shows a basic vector add +1 (constant) by manipulating
#         memory inplace.
*******************************************************************************/

#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <assert.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <CL/opencl.h>
#include <CL/cl_ext.h>
#include "xclhal2.h"
#include <xrt.h>

////////////////////////////////////////////////////////////////////////////////

#define NUM_WORKGROUPS (1)
#define WORKGROUP_SIZE (256)
#define MAX_LENGTH 8192
#define MEM_ALIGNMENT 4096
#if defined(VITIS_PLATFORM) && !defined(TARGET_DEVICE)
#define STR_VALUE(arg)      #arg
#define GET_STRING(name) STR_VALUE(name)
#define TARGET_DEVICE GET_STRING(VITIS_PLATFORM)
#endif

////////////////////////////////////////////////////////////////////////////////

cl_uint load_file_to_memory(const char *filename, char **result)
{
    cl_uint size = 0;
    FILE *f = fopen(filename, "rb");
    if (f == NULL) {
        *result = NULL;
        return -1; // -1 means file opening fail
    }
    fseek(f, 0, SEEK_END);
    size = ftell(f);
    fseek(f, 0, SEEK_SET);
    *result = (char *)malloc(size+1);
    if (size != fread(*result, sizeof(char), size, f)) {
        free(*result);
        return -2; // -2 means file reading fail
    }
    fclose(f);
    (*result)[size] = 0;
    return size;
}

int main(int argc, char** argv)
{

    cl_int err;                            // error code returned from api calls
    cl_uint check_status = 0;
    const cl_uint number_of_words = 4096; // 16KB of data


    cl_platform_id platform_id;         // platform id
    cl_device_id device_id;             // compute device id
    cl_context context;                 // compute context
    cl_command_queue commands;          // compute command queue
    cl_program program;                 // compute programs
    cl_kernel kernel;                   // compute kernel

    //xrtDeviceHandle xrt_dev_handle = xrtDeviceOpen (1); // our U280 usually sits at index 1
    xclDeviceHandle xcl_dev_handle = xclOpen(1, "xclopen.log", XCL_INFO);

    cl_uint* h_data;                                // host memory for input vector
    char cl_platform_vendor[1001];
    char target_device_name[1001] = TARGET_DEVICE;

    cl_uint* h_axi00_ptr0_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_axi00_ptr0;                         // device memory used for a vector

    cl_uint* h_axi01_ptr0_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_axi01_ptr0;                         // device memory used for a vector

    cl_uint* h_axi02_ptr0_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_axi02_ptr0;                         // device memory used for a vector

    if (argc != 3) {
        printf("Usage: %s <xclbin> <TTA bin2n program image>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // Fill our data sets with pattern
    h_data = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*));
    for(cl_uint i = 0; i < MAX_LENGTH; i++) {
        h_data[i]  = i;
        h_axi00_ptr0_output[i] = 0; 
        h_axi01_ptr0_output[i] = 0; 
        h_axi02_ptr0_output[i] = 0; 

    }

    // Get all platforms and then select Xilinx platform
    cl_platform_id platforms[16];       // platform id
    cl_uint platform_count;
    cl_uint platform_found = 0;
    err = clGetPlatformIDs(16, platforms, &platform_count);
    if (err != CL_SUCCESS) {
        printf("ERROR: Failed to find an OpenCL platform!\n");
        printf("ERROR: Test failed\n");
        return EXIT_FAILURE;
    }
    printf("INFO: Found %d platforms\n", platform_count);

    // Find Xilinx Plaftorm
    for (cl_uint iplat=0; iplat<platform_count; iplat++) {
        err = clGetPlatformInfo(platforms[iplat], CL_PLATFORM_VENDOR, 1000, (void *)cl_platform_vendor,NULL);
        if (err != CL_SUCCESS) {
            printf("ERROR: clGetPlatformInfo(CL_PLATFORM_VENDOR) failed!\n");
            printf("ERROR: Test failed\n");
            return EXIT_FAILURE;
        }
        if (strcmp(cl_platform_vendor, "Xilinx") == 0) {
            printf("INFO: Selected platform %d from %s\n", iplat, cl_platform_vendor);
            platform_id = platforms[iplat];
            platform_found = 1;
        }
    }
    if (!platform_found) {
        printf("ERROR: Platform Xilinx not found. Exit.\n");
        return EXIT_FAILURE;
    }

    // Get Accelerator compute device
    cl_uint num_devices;
    cl_uint device_found = 0;
    cl_device_id devices[16];  // compute device id
    char cl_device_name[1001];
    err = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_ACCELERATOR, 16, devices, &num_devices);
    printf("INFO: Found %d devices\n", num_devices);
    if (err != CL_SUCCESS) {
        printf("ERROR: Failed to create a device group!\n");
        printf("ERROR: Test failed\n");
        return -1;
    }

    //iterate all devices to select the target device.
    for (cl_uint i=0; i<num_devices; i++) {
        err = clGetDeviceInfo(devices[i], CL_DEVICE_NAME, 1024, cl_device_name, 0);
        if (err != CL_SUCCESS) {
            printf("ERROR: Failed to get device name for device %d!\n", i);
            printf("ERROR: Test failed\n");
            return EXIT_FAILURE;
        }
        printf("CL_DEVICE_NAME %s\n", cl_device_name);
        if(strcmp(cl_device_name, target_device_name) == 0) {
            device_id = devices[i];
            device_found = 1;
            printf("Selected %s as the target device\n", cl_device_name);
        }
    }

    if (!device_found) {
        printf("ERROR:Target device %s not found. Exit.\n", target_device_name);
        return EXIT_FAILURE;
    }

    // Create a compute context
    printf("Creating compute context\n");
    context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
    if (!context) {
        printf("ERROR: Failed to create a compute context!\n");
        printf("ERROR: Test failed\n");
        return EXIT_FAILURE;
    }

    // Create a command commands
    printf("Creating commands queues\n");
    commands = clCreateCommandQueue(context, device_id, CL_QUEUE_PROFILING_ENABLE | CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE, &err);
    if (!commands) {
        printf("ERROR: Failed to create a command commands!\n");
        printf("ERROR: code %i\n",err);
        printf("ERROR: Test failed\n");
        return EXIT_FAILURE;
    }

    cl_int status;

    // Create Program Objects
    // Load binary from disk
    unsigned char *kernelbinary;
    char *xclbin = argv[1];

    //------------------------------------------------------------------------------
    // xclbin
    //------------------------------------------------------------------------------
    printf("INFO: loading xclbin %s\n", xclbin);
    cl_uint n_i0 = load_file_to_memory(xclbin, (char **) &kernelbinary);
    if (n_i0 < 0) {
        printf("ERROR: failed to load kernel from xclbin: %s\n", xclbin);
        printf("ERROR: Test failed\n");
        return EXIT_FAILURE;
    }

    size_t n0 = n_i0;

    // Create the compute program from offline
    printf("Creating compute program\n");
    program = clCreateProgramWithBinary(context, 1, &device_id, &n0,
                                        (const unsigned char **) &kernelbinary, &status, &err);
    free(kernelbinary);

    if ((!program) || (err!=CL_SUCCESS)) {
        printf("ERROR: Failed to create compute program from binary %d!\n", err);
        printf("ERROR: Test failed\n");
        return EXIT_FAILURE;
    }


    // Build the program executable
    printf("Building program executable\n");
    err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    if (err != CL_SUCCESS) {
        size_t len;
        char buffer[2048];

        printf("ERROR: Failed to build program executable!\n");
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
        printf("%s\n", buffer);
        printf("ERROR: Test failed\n");
        return EXIT_FAILURE;
    }

    // Create the compute kernel in the program we wish to run
    printf("Creating compute kernel\n");
    kernel = clCreateKernel(program, "hbm_tta_vec2add", &err);
    if (!kernel || err != CL_SUCCESS) {
        printf("ERROR: Failed to create compute kernel!\n");
        printf("ERROR: Test failed\n");
        return EXIT_FAILURE;
    }

    // Create structs to define memory bank mapping
    cl_mem_ext_ptr_t mem_ext;
    mem_ext.obj = NULL;
    mem_ext.param = kernel;

    printf("Creating HBM buffers\n");
    mem_ext.flags = 4;
    d_axi00_ptr0 = clCreateBuffer(context,  CL_MEM_WRITE_ONLY | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for axi00 clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 5;
    d_axi01_ptr0 = clCreateBuffer(context,  CL_MEM_WRITE_ONLY | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for axi01 clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 6;
    d_axi02_ptr0 = clCreateBuffer(context,  CL_MEM_READ_ONLY | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for axi02 clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    if (!(d_axi00_ptr0&&d_axi01_ptr0&&d_axi02_ptr0)) {
        printf("ERROR: Failed to allocate device memory!\n");
        printf("ERROR: Test failed\n");
        return EXIT_FAILURE;
    }

    printf("Writing to axi00 buffer\n");
    err = clEnqueueWriteBuffer(commands, d_axi00_ptr0, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("ERROR: Failed to write to source array h_data: d_axi00_ptr0: %d!\n", err);
        printf("ERROR: Test failed\n");
        return EXIT_FAILURE;
    }

    printf("Writing to axi01 buffer\n");
    err = clEnqueueWriteBuffer(commands, d_axi01_ptr0, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("ERROR: Failed to write to source array h_data: d_axi01_ptr0: %d!\n", err);
        printf("ERROR: Test failed\n");
        return EXIT_FAILURE;
    }

/*  axi02 is now a read buffer for the results
    err = clEnqueueWriteBuffer(commands, d_axi02_ptr0, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("ERROR: Failed to write to source array h_data: d_axi02_ptr0: %d!\n", err);
        printf("ERROR: Test failed\n");
        return EXIT_FAILURE;
    }
*/

    // Set the arguments to our compute kernel
    printf("Setting scalar arguments\n");
    // cl_uint vector_length = MAX_LENGTH;
    err = 0;
    cl_uint d_scalar00 = 0;
    err |= clSetKernelArg(kernel, 0, sizeof(cl_uint), &d_scalar00); // Not used in example RTL logic.
    cl_uint d_scalar01 = 0;
    err |= clSetKernelArg(kernel, 1, sizeof(cl_uint), &d_scalar01); // Not used in example RTL logic.
    cl_uint d_scalar02 = 0;
    err |= clSetKernelArg(kernel, 2, sizeof(cl_uint), &d_scalar02); // Not used in example RTL logic.
    cl_uint d_scalar03 = 0;
    err |= clSetKernelArg(kernel, 3, sizeof(cl_uint), &d_scalar03); // Not used in example RTL logic.
    err |= clSetKernelArg(kernel, 4, sizeof(cl_mem), &d_axi00_ptr0); 
    err |= clSetKernelArg(kernel, 5, sizeof(cl_mem), &d_axi01_ptr0); 
    err |= clSetKernelArg(kernel, 6, sizeof(cl_mem), &d_axi02_ptr0); 

    if (err != CL_SUCCESS) {
        printf("ERROR: Failed to set kernel arguments! %d\n", err);
        printf("ERROR: Test failed\n");
        return EXIT_FAILURE;
    }
    

    //Get TTA core info
    printf("Reading TTA core info\n");
/*
 * info registers space: 0xC0..0xff
TTA_DEVICECLASS
TTA_DEVICE_ID
TTA_INTERFACE_TYPE
TTA_CORE_COUNT
TTA_CTRL_SIZE
TTA_DMEM_SIZE
TTA_IMEM_SIZE
TTA_PMEM_SIZE
*/

//test shared BRAM access

    int kernel_base = 0x1800000; //read from xclbin

    int tta_offset = 0x40000;
    int tta_base = kernel_base + tta_offset;
    int max_addr_width = 16;
    int imem_address = tta_base + (01 << max_addr_width);
    int ctrl_address = tta_base + (128 * 4); //(128 = 2^7), see almaif RTL code
    int stat_address = tta_base + ((128 + 64) * 4); //128 + 64 = 2^7 + 2^6


//    int imem_address = kernel_base + tta_offset;

    size_t ret = 0;
    char tta_info_buf[8 * 4];
    //for (int memregion = 0 ; memregion < 4; memregion++) {
    //    int base = tta_base + (memregion << max_addr_width);
    //    ret = xclRead(xcl_dev_handle, XCL_ADDR_KERNEL_CTRL, base, tta_info_buf, 8 * 4);
    //    printf("xclRead return value: %d\n", ret);
    //    for (int i = 0; i < 8; i++) {
    //        printf("TTA memregion base 0x%08x info[%d]: 0x%08x\n", base, i, ((int*)tta_info_buf)[i]);
    //    }
    //}
    ret = xclRead(xcl_dev_handle, XCL_ADDR_KERNEL_CTRL, stat_address, tta_info_buf, 8 * 4);
    printf("xclRead return value: %d\n", ret);
    for (int i = 0; i < 8; i++) {
        printf("TTA status regs at address 0x%08x: 0x%08x\n", stat_address + (i*4), ((int*)tta_info_buf)[i]);
    }

    printf("Writing TTA instruction memory\n");

    int fd = open (argv[2], O_RDONLY);
    if (fd < 0) {
        printf("Error opening TTA program image (%s)\n", argv[2]);
        return EXIT_FAILURE;
    }
    struct stat fstatus;
    int retval = fstat (fd, &fstatus);
    if (retval < 0) {
        printf("fstat failed on file %s\n", argv[2]);
        return EXIT_FAILURE;
    }
    int tta_prog_size = fstatus.st_size;
    if (tta_prog_size > (64 * 1024)) {
        printf("Your TTA program image is too large.\n");
        return EXIT_FAILURE;
    }
    const char *mapped_tta_prog = (const char*)mmap (0, tta_prog_size, PROT_READ, MAP_PRIVATE, fd, 0);
    if (mapped_tta_prog == MAP_FAILED) {
        printf("mmap TTA program image failed (file: %s)\n", argv[2]);
        return EXIT_FAILURE;
    }
   
    // Perform the write
    ret = xclWrite(xcl_dev_handle, XCL_ADDR_KERNEL_CTRL, imem_address, mapped_tta_prog, tta_prog_size);
    printf("xclWrite return value: %d\n", ret);

    // read the memregions again (see if one has changed)
    for (int memregion = 0 ; memregion < 2; memregion++) {
        int base = tta_base + (memregion << max_addr_width);
        ret = xclRead(xcl_dev_handle, XCL_ADDR_KERNEL_CTRL, base, tta_info_buf, 8 * 4);
        printf("xclRead return value: %d\n", ret);
        for (int i = 0; i < 8; i++) {
            printf("TTA memregion base 0x%08x info[%d]: 0x%08x\n", base, i, ((int*)tta_info_buf)[i]);
        }
    }

    ret = xclRead(xcl_dev_handle, XCL_ADDR_KERNEL_CTRL, kernel_base, tta_info_buf, 8 * 4);
    printf("xclRead return value: %d\n", ret);
    for (int i = 0; i < 8; i++) {
        printf("kernel base mem[%d]: 0x%08x\n", i, ((int*)tta_info_buf)[i]);
    }

    //Start the TTA core
    const int writeVal[] = {2};
    ret = xclWrite(xcl_dev_handle, XCL_ADDR_KERNEL_CTRL, ctrl_address, writeVal, 1);
    printf("xclWrite to address 0x%08x return value: %d\n", ctrl_address, ret);

    // Read the ctrl registers again
    ret = xclRead(xcl_dev_handle, XCL_ADDR_KERNEL_CTRL, stat_address, tta_info_buf, 8 * 4);
    printf("xclRead return value: %d\n", ret);
    for (int i = 0; i < 8; i++) {
        printf("TTA status regs at address 0x%08x: 0x%08x\n", stat_address + (i*4), ((int*)tta_info_buf)[i]);
    }


    /*
   xclOpenContext(device_handle, xclbin_id, cu_index, false);
   xclRegRead(device_handle, cu_index, offset, &data);
   xclRegWrite(device_handle, cu_index, offset, data);
   xclCloseContext(device_handle, xclbin_id, cu_index);
   */

    size_t global[1];
    size_t local[1];
    // Execute the kernel over the entire range of our 1d input data set
    // using the maximum number of work group items for this device

    global[0] = 1;
    local[0] = 1;
    printf("Enqueueing kernel\n");
    err = clEnqueueNDRangeKernel(commands, kernel, 1, NULL, (size_t*)&global, (size_t*)&local, 0, NULL, NULL);
    if (err) {
        printf("ERROR: Failed to execute kernel! %d\n", err);
        printf("ERROR: Test failed\n");
        return EXIT_FAILURE;
    }

    //clFinish(commands);

#if 0
    // Read back the results from the device to verify the output
    //
    cl_event readevent;

    printf("Reading back buffers\n");
    err = 0;
    printf("enqueueing readbuffer command for axi00\n");
    err |= clEnqueueReadBuffer( commands, d_axi00_ptr0, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_axi00_ptr0_output, 0, NULL, &readevent );
    printf("enqueueing readbuffer command for axi01\n");

    err |= clEnqueueReadBuffer( commands, d_axi01_ptr0, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_axi01_ptr0_output, 0, NULL, &readevent );
    printf("enqueueing readbuffer command for axi02\n");

    err |= clEnqueueReadBuffer( commands, d_axi02_ptr0, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_axi02_ptr0_output, 0, NULL, &readevent );


    if (err != CL_SUCCESS) {
        printf("ERROR: Failed to read output array! %d\n", err);
        printf("ERROR: Test failed\n");
        return EXIT_FAILURE;
    }
    clWaitForEvents(1, &readevent);
    // Check Results

    for (cl_uint i = 0; i < number_of_words; i++) {
        if ((h_data[i] + 1) != h_axi00_ptr0_output[i]) {
            printf("ERROR in hbm_tta_vec2add::m00_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_axi00_ptr0_output[i], h_axi00_ptr0_output[i]);
            check_status = 1;
        }
      //  printf("i=%d, input=%d, output=%d\n", i,  h_axi00_ptr0_input[i], h_axi00_ptr0_output[i]);
    }


    for (cl_uint i = 0; i < number_of_words; i++) {
        if ((h_data[i] + 1) != h_axi01_ptr0_output[i]) {
            printf("ERROR in hbm_tta_vec2add::m01_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_axi01_ptr0_output[i], h_axi01_ptr0_output[i]);
            check_status = 1;
        }
      //  printf("i=%d, input=%d, output=%d\n", i,  h_axi01_ptr0_input[i], h_axi01_ptr0_output[i]);
    }


    for (cl_uint i = 0; i < number_of_words; i++) {
        if ((h_data[i] + 1) != h_axi02_ptr0_output[i]) {
            printf("ERROR in hbm_tta_vec2add::m02_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_axi02_ptr0_output[i], h_axi02_ptr0_output[i]);
            check_status = 1;
        }
      //  printf("i=%d, input=%d, output=%d\n", i,  h_axi02_ptr0_input[i], h_axi02_ptr0_output[i]);
    }

#endif

    //--------------------------------------------------------------------------
    // Shutdown and cleanup
    //-------------------------------------------------------------------------- 
    clReleaseMemObject(d_axi00_ptr0);
    free(h_axi00_ptr0_output);

    clReleaseMemObject(d_axi01_ptr0);
    free(h_axi01_ptr0_output);

    clReleaseMemObject(d_axi02_ptr0);
    free(h_axi02_ptr0_output);



    free(h_data);
    clReleaseProgram(program);
    clReleaseKernel(kernel);
    clReleaseCommandQueue(commands);
    clReleaseContext(context);

    if (check_status) {
        printf("ERROR: Test failed\n");
        return EXIT_FAILURE;
    } else {
        printf("INFO: Test completed successfully.\n");
        return EXIT_SUCCESS;
    }


} // end of main
