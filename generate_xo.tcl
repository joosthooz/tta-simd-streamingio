#TODO: create an argument for this
#set project_name [lindex $argv 0]
set ttaproject_name hbm_tta_vec2add
append ttaproject_dir $ttaproject_name _gen

#If the project is not generated yet, create it with this .tcl file.
#The Makefile will do this as a dependency
#source hbm_tta_vec2add.tcl
open_project $ttaproject_dir/$ttaproject_dir.xpr
update_compile_order -fileset sources_1
#JH added commands to add the import files in the expected location, and create the export folder where the .xo is created
#exec ln -sf ../vivado_srcs/hbm_tta_vec2add/imports $ttaproject_dir/imports #hm doesn't seem to be working as intended
exec mkdir -p $ttaproject_dir/exports

# Generate the block design
reset_target all [get_files  $ttaproject_dir/$ttaproject_dir.srcs/sources_1/bd/${ttaproject_name}_bd/${ttaproject_name}_bd.bd]
export_ip_user_files -of_objects  [get_files  $ttaproject_dir/$ttaproject_dir.srcs/sources_1/bd/${ttaproject_name}_bd/${ttaproject_name}_bd.bd] -sync -no_script -force -quiet
delete_ip_run [get_files -of_objects [get_fileset ${ttaproject_name}_bd] $ttaproject_dir/$ttaproject_dir.srcs/sources_1/bd/${ttaproject_name}_bd/${ttaproject_name}_bd.bd]
update_compile_order -fileset sources_1
generate_target all [get_files  $ttaproject_dir/$ttaproject_dir.srcs/sources_1/bd/${ttaproject_name}_bd/${ttaproject_name}_bd.bd]
export_ip_user_files -of_objects [get_files $ttaproject_dir/$ttaproject_dir.srcs/sources_1/bd/${ttaproject_name}_bd/${ttaproject_name}_bd.bd] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] $ttaproject_dir/$ttaproject_dir.srcs/sources_1/bd/${ttaproject_name}_bd/${ttaproject_name}_bd.bd]
launch_runs hbm_tta_vec2add_bd_synth_1 -jobs 4
#export_simulation -of_objects [get_files /work/jjhoozemans/HBM/TTA/newrepo/tta-simd-streamingio/hbm_tta_vec2add_gen/hbm_tta_vec2add_gen.srcs/sources_1/bd/hbm_tta_vec2add_bd/hbm_tta_vec2add_bd.bd] -directory /work/jjhoozemans/HBM/TTA/newrepo/tta-simd-streamingio/hbm_tta_vec2add_gen/hbm_tta_vec2add_gen.ip_user_files/sim_scripts -ip_user_files_dir /work/jjhoozemans/HBM/TTA/newrepo/tta-simd-streamingio/hbm_tta_vec2add_gen/hbm_tta_vec2add_gen.ip_user_files -ipstatic_source_dir /work/jjhoozemans/HBM/TTA/newrepo/tta-simd-streamingio/hbm_tta_vec2add_gen/hbm_tta_vec2add_gen.ip_user_files/ipstatic -lib_map_path [list {modelsim=/work/jjhoozemans/HBM/TTA/newrepo/tta-simd-streamingio/hbm_tta_vec2add_gen/hbm_tta_vec2add_gen.cache/compile_simlib/modelsim} {questa=/work/jjhoozemans/HBM/TTA/newrepo/tta-simd-streamingio/hbm_tta_vec2add_gen/hbm_tta_vec2add_gen.cache/compile_simlib/questa} {ies=/work/jjhoozemans/HBM/TTA/newrepo/tta-simd-streamingio/hbm_tta_vec2add_gen/hbm_tta_vec2add_gen.cache/compile_simlib/ies} {xcelium=/work/jjhoozemans/HBM/TTA/newrepo/tta-simd-streamingio/hbm_tta_vec2add_gen/hbm_tta_vec2add_gen.cache/compile_simlib/xcelium} {vcs=/work/jjhoozemans/HBM/TTA/newrepo/tta-simd-streamingio/hbm_tta_vec2add_gen/hbm_tta_vec2add_gen.cache/compile_simlib/vcs} {riviera=/work/jjhoozemans/HBM/TTA/newrepo/tta-simd-streamingio/hbm_tta_vec2add_gen/hbm_tta_vec2add_gen.cache/compile_simlib/riviera}] -use_ip_compiled_libs -force -quiet


# --------------------------------------------
# Start: RTL Kernel Packaging of Netlist
#
source -notrace vivado_srcs/$ttaproject_name/imports/package_kernel.tcl
# Running synthesis
reset_run synth_1
launch_runs synth_1 -jobs 4
wait_on_run [get_runs synth_1]
open_run synth_1 -name synth_1
rename_ref -prefix_all hbm_tta_vec2add_
write_checkpoint $ttaproject_dir/$ttaproject_dir.runs/synth_1/packaged.dcp
write_xdc $ttaproject_dir/$ttaproject_dir.runs/synth_1/packaged.xdc
close_design
package_project_dcp_and_xdc $ttaproject_dir/$ttaproject_dir.runs/synth_1/packaged.dcp $ttaproject_dir/$ttaproject_dir.runs/synth_1/packaged.xdc $ttaproject_dir/$ttaproject_name tud_tut kernel $ttaproject_name
package_xo  -xo_path $ttaproject_dir/exports/$ttaproject_name.xo -kernel_name $ttaproject_name -ip_directory $ttaproject_dir/$ttaproject_name -kernel_xml vivado_srcs/$ttaproject_name/imports/kernel.xml

#package_xo  -xo_path vivado_srcs/hbm_tta_vec2add/exports/hbm_tta_vec2add.xo -kernel_name hbm_tta_vec2add -kernel_xml vivado_srcs/hbm_tta_vec2add/imports/kernel.xml
