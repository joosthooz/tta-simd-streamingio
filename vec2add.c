//#define DEBUG

#ifdef DEBUG
#include <stdio.h>
#undef DEBUG
#define DEBUG(...) fprintf(stderr, __VA_ARGS__)
#else
#define DEBUG(...)
#endif

#include "vec3i3o.h"

int vec3i3o(int8 (*pop1)(void), int8 (*pop2)(void), int8 (*pop3)(void), void (*push1)(int8), void (*push2)(int8), void (*push3)(int8)) {

  while (1) {
  int8 in1 = pop1();
  int8 in2 = pop2();
  int8 out = in1 + in2;
  push3(out);
  in1 = pop1();
  in2 = pop2();
  out = in1 + in2;
  push3(out);
  in1 = pop1();
  in2 = pop2();
  out = in1 + in2;
  push3(out);
  in1 = pop1();
  in2 = pop2();
  out = in1 + in2;
  push3(out);
  in1 = pop1();
  in2 = pop2();
  out = in1 + in2;
  push3(out);
  in1 = pop1();
  in2 = pop2();
  out = in1 + in2;
  push3(out);
  in1 = pop1();
  in2 = pop2();
  out = in1 + in2;
  push3(out);
  in1 = pop1();
  in2 = pop2();
  out = in1 + in2;
  push3(out);
  in1 = pop1();
  in2 = pop2();
  out = in1 + in2;
  push3(out);
  in1 = pop1();
  in2 = pop2();
  out = in1 + in2;
  push3(out);
  in1 = pop1();
  in2 = pop2();
  out = in1 + in2;
  push3(out);
  in1 = pop1();
  in2 = pop2();
  out = in1 + in2;
  push3(out);
  }
  return 0;
}
