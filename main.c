#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <tce_vector.h>
#include "vec3i3o.h"

int8 input1() {
  //Code for when you'd wish to first check if there is data availeble.
/*
  int status;
  while (1)
  {
    _TCE_STREAM_IN_STATUS(0, status);
    if (status == 1)
    break;
  }
*/
  int8 val;
  _TCEFU_STREAM_IN_256("Streamin1", 0, val);
  return val;
}

int8 input2() {
  int8 val;
  _TCEFU_STREAM_IN_256("Streamin2", 0, val);
  return val;
}

int8 input3() {
  int8 val;
  _TCEFU_STREAM_IN_256("Streamin3", 0, val);
  return val;
}


void output1(int8 val) {
  _TCEFU_STREAM_OUT_256("Streamout", val);
}

void output2(int8 val) {
  _TCEFU_STREAM_OUT_256("Streamout2", val);
}

void output3(int8 val) {
  _TCEFU_STREAM_OUT_256("Streamout3", val);
}

int main(int argc, char *argv[]) {
  int retval = vec3i3o(input1, input2, input3, output1, output2, output3);
  return retval;
}
