#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>


int main(int argc, char *argv[]) {
	if (argc != 2) {
		printf("Usage: %s <nbytes>", argv[0]);
		printf("Generate sequential values of data in binary little-endian 32bit integer format");
	}
	int n = strtol(argv[1], 0, 10);
	if (n%4) {
		n += 4 - (n%4);
	}
	uint32_t *buf = (uint32_t*)malloc(n * sizeof(uint32_t));
	for (int i = 0; i < n; i++) {
		buf[i] = i;
	}
	write(STDOUT_FILENO, buf, n * sizeof(uint32_t));
	return 0;
}
