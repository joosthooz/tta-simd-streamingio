ifndef ADF_FILE
ADF_FILE=simtta-32x8-3i3o.adf
endif

CORENAME=streaming_simd_tta

PROGRAMNAME?=vec2add
SYNTH_TARGET?=hw

NDATAVALS=32

DSDB=autoexplorer.dsdb

OPT=-O3

TCE_STREAM_ENABLED=default_to_this

TCE_OUTFILE=outputfile
ifdef TCE_STREAM_ENABLED
TCE_STREAM=-DTCE_STREAM
TCE_OUTFILE=Streamout.out
else
TCE_OUTFILE=outputfile
TTASIM_WRITE=load_data /a data 0x800000 Streamin.in;
TTASIM_READ=x /a data /f outputfile /n 505609 0x900000;
endif

#MOVE_STACK=--init-sp=4088
#WRITE_DONE=-DWRITE_DONE
TCE_SCHEDULER=--enable-loop-scheduler
TCE_OPT=-g -Wall ${TCE_SCHEDULER} ${MOVE_STACK} \
	-d ${FULL_CUSTOP} ${WRITE_DONE} ${TCE_STREAM} ${TCE_MEM_SHORTCUT}


ICDARGS=--icd-arg-list=debugger:minimal,fpga-optimized:yes#,bypasspcregister:yes #(for 2 delay slots!) #,bustrace:yes,bustracestartingcycle:4
HDBS=--hdb-list=stream256.hdb,xilinx_series7.hdb,almaif.hdb,generate_base32.hdb,generate_base_simd.hdb,generate_rf_iu.hdb


.PHONY: all run_tta run_rtl

all: tta_rtl #run_tta run_rtl

run_tta: ${PROGRAMNAME}.tpef ${PROGRAMNAME}_data.in.bin
	ln -sf ${PROGRAMNAME}_data.in.bin Streamin1.in
	ln -sf ${PROGRAMNAME}_data.in.bin Streamin2.in
	ln -sf ${PROGRAMNAME}_data.in.bin Streamin3.in
	hexdump -C ${PROGRAMNAME}_data.in.bin
	ttasim -a ${ADF_FILE} -e "prog ${PROGRAMNAME}.tpef; ${TTASIM_WRITE} run; puts [info proc cycles]; ${TTASIM_READ} exit"
	hexdump -C Streamout.out
	md5sum ${TCE_OUTFILE}

tta_rtl: ${ADF_FILE} ${PROGRAMNAME}.tpef # --fu-middle-register=all
	[ ! -d tta_rtl ] || rm -r tta_rtl
	generateprocessor  -o $@ -e ${CORENAME} -g AlmaIFIntegrator -f onchip -d onchip -p ${PROGRAMNAME}.tpef ${ICDARGS} ${HDBS} ${ADF_FILE}
	#cd tta_rtl; generatebits -x ./ -f vhdl -e ${CORENAME} -p ../${PROGRAMNAME}.tpef ../${ADF_FILE}
	generatebits -x ./tta_rtl -f bin2n -e ${CORENAME} -p ${PROGRAMNAME}.tpef --output-file ${PROGRAMNAME}.img ${ADF_FILE}
	cp xilinx_dp_blockram.vhdl tta_rtl/platform #Apply workaround for instruction memory padding

run_rtl: tta_rtl ${PROGRAMNAME}_data.in
	./run_rtl_sim.sh

${PROGRAMNAME}.tpef: ${ADF_FILE} ${PROGRAMNAME}.c main.c
	tcecc ${OPT} ${TCE_OPT} -a $^ -o $@

%.tpef.S: %.tpef
	tcedisasm -n ${ADF_FILE} $^

%.bin: genbindata
	./genbindata ${NDATAVALS} > $@

%.img.hex: %.img
	xxd -p -c 4 $< > $@

PWD := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

VIVADO_PROJECT_FOLDER = hbm_tta_vec2add_gen
XILINX_OBJECT = $(VIVADO_PROJECT_FOLDER)/exports/$(KERNEL_NAME).xo
BUILD_DIR = $(PWD)/xclbin_build
LOG_DIR := $(BUILD_DIR)/logs
XCLBIN_OUT = $(BUILD_DIR)/$(KERNEL_NAME).xclbin
DEVICE ?= xilinx_u280_xdma_201920_3
KERNEL_NAME = hbm_tta_vec2add

vivado_project: ${VIVADO_PROJECT_FOLDER}
${VIVADO_PROJECT_FOLDER}:
	vivado -source hbm_tta_vec2add.tcl

build: xclbin
xo: ${XILINX_OBJECT}

${XILINX_OBJECT}: ${VIVADO_PROJECT_FOLDER}
	rm -f $(VIVADO_PROJECT_FOLDER)/$(KERNEL_NAME)/impl/packaged.xdc 
	rm -f $(VIVADO_PROJECT_FOLDER)/$(KERNEL_NAME)/synth_1/packaged.xdc
	rm -f $@ #remove the kernel object if sources are newer
	vivado -mode batch -source generate_xo.tcl

xclbin: $(XCLBIN_OUT)
$(XCLBIN_OUT): $(XILINX_OBJECT)
	mkdir -p $(BUILD_DIR)
	mkdir -p $(LOG_DIR)
	v++ -R R1 -l -t ${SYNTH_TARGET} --platform ${DEVICE} --config bank_config.ini --kernel_frequency 250 --temp_dir $(BUILD_DIR) --log_dir $(LOG_DIR) -o $(XCLBIN_OUT) $(XILINX_OBJECT)
	mv xrc.log $(LOG_DIR) 
	mv xcd.log $(LOG_DIR)

.PHONY: clean
clean:
	rm -f data.* *.tpef *.out *.img
