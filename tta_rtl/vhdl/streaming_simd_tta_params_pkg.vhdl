package streaming_simd_tta_params is
  constant fu_PARAM_LSU_addrw_g : integer := 12;
  constant fu_DATA_LSU_addrw_g : integer := 15;
end streaming_simd_tta_params;
