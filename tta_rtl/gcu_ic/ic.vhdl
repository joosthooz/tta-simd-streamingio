library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.ext;
use IEEE.std_logic_arith.sxt;
use work.streaming_simd_tta_globals.all;
use work.tce_util.all;

entity streaming_simd_tta_interconn is

  port (
    clk : in std_logic;
    rstx : in std_logic;
    glock : in std_logic;
    socket_bool_i1_data : out std_logic_vector(0 downto 0);
    socket_bool_i1_bus_cntrl : in std_logic_vector(0 downto 0);
    socket_gcu_i1_data : out std_logic_vector(IMEMADDRWIDTH-1 downto 0);
    socket_gcu_i1_bus_cntrl : in std_logic_vector(0 downto 0);
    socket_gcu_i2_data : out std_logic_vector(IMEMADDRWIDTH-1 downto 0);
    socket_gcu_i2_bus_cntrl : in std_logic_vector(0 downto 0);
    socket_ALU_i1_data : out std_logic_vector(31 downto 0);
    socket_ALU_i1_bus_cntrl : in std_logic_vector(0 downto 0);
    socket_ALU_i2_data : out std_logic_vector(31 downto 0);
    socket_ALU_i2_bus_cntrl : in std_logic_vector(0 downto 0);
    socket_W1_32_data : out std_logic_vector(31 downto 0);
    socket_ALU_i1_4_data : out std_logic_vector(31 downto 0);
    socket_ALU_i1_1_1_1_1_data : out std_logic_vector(14 downto 0);
    socket_ALU_i1_1_1_1_1_bus_cntrl : in std_logic_vector(0 downto 0);
    socket_ALU_o1_1_1_2_1_data : out std_logic_vector(255 downto 0);
    socket_ALU_o1_1_1_2_1_bus_cntrl : in std_logic_vector(1 downto 0);
    socket_ALU_o1_1_1_2_1_1_data : out std_logic_vector(255 downto 0);
    socket_ALU_o1_1_1_2_1_1_bus_cntrl : in std_logic_vector(0 downto 0);
    socket_ALU_o1_1_1_2_1_2_data : out std_logic_vector(255 downto 0);
    socket_ALU_o1_1_1_2_1_2_bus_cntrl : in std_logic_vector(0 downto 0);
    socket_W1_32_1_data : out std_logic_vector(255 downto 0);
    socket_W1_32_1_bus_cntrl : in std_logic_vector(0 downto 0);
    socket_ALU_i1_1_1_1_1_1_1_data : out std_logic_vector(255 downto 0);
    socket_ALU_i1_1_1_1_1_1_1_bus_cntrl : in std_logic_vector(0 downto 0);
    socket_ALU_i1_1_1_1_2_1_data : out std_logic_vector(255 downto 0);
    socket_ALU_i1_1_1_1_2_1_bus_cntrl : in std_logic_vector(1 downto 0);
    socket_ALU_i1_1_1_1_1_1_2_5_data : out std_logic_vector(255 downto 0);
    socket_ALU_i1_1_1_1_1_1_2_6_data : out std_logic_vector(31 downto 0);
    socket_ALU_i1_1_1_1_1_2_data : out std_logic_vector(31 downto 0);
    socket_ALU_i1_1_1_1_3_data : out std_logic_vector(11 downto 0);
    socket_ALU_i2_1_data : out std_logic_vector(31 downto 0);
    socket_ALU_i2_1_bus_cntrl : in std_logic_vector(0 downto 0);
    socket_ALU_i2_2_data : out std_logic_vector(31 downto 0);
    socket_ALU_i2_2_bus_cntrl : in std_logic_vector(0 downto 0);
    socket_ALU_i1_1_1_1_2_data : out std_logic_vector(31 downto 0);
    socket_ALU_i1_1_1_1_2_bus_cntrl : in std_logic_vector(0 downto 0);
    socket_StreamSock1_data : out std_logic_vector(31 downto 0);
    socket_StreamSock1_bus_cntrl : in std_logic_vector(1 downto 0);
    socket_StreamSock2_data : out std_logic_vector(31 downto 0);
    socket_StreamSock2_bus_cntrl : in std_logic_vector(1 downto 0);
    socket_StreamSockV3_data : out std_logic_vector(255 downto 0);
    socket_StreamSockV3_bus_cntrl : in std_logic_vector(1 downto 0);
    socket_StreamSockV3_1_data : out std_logic_vector(255 downto 0);
    socket_StreamSockV3_1_bus_cntrl : in std_logic_vector(1 downto 0);
    socket_StreamSockV3_2_data : out std_logic_vector(255 downto 0);
    socket_StreamSockV3_2_bus_cntrl : in std_logic_vector(1 downto 0);
    socket_StreamSock2_1_data : out std_logic_vector(31 downto 0);
    socket_StreamSock2_1_bus_cntrl : in std_logic_vector(1 downto 0);
    socket_ALU_o1_1_1_2_1_2_1_data : out std_logic_vector(255 downto 0);
    socket_ALU_o1_1_1_2_1_2_1_bus_cntrl : in std_logic_vector(0 downto 0);
    socket_ALU_o1_1_1_2_1_1_1_data : out std_logic_vector(255 downto 0);
    socket_ALU_o1_1_1_2_1_1_1_bus_cntrl : in std_logic_vector(0 downto 0);
    socket_ALU_o1_1_1_2_1_3_data : out std_logic_vector(255 downto 0);
    socket_ALU_o1_1_1_2_1_3_bus_cntrl : in std_logic_vector(1 downto 0);
    B1_mux_ctrl_in : in std_logic_vector(3 downto 0);
    B1_data_0_in : in std_logic_vector(0 downto 0);
    B1_data_1_in : in std_logic_vector(IMEMADDRWIDTH-1 downto 0);
    B1_data_2_in : in std_logic_vector(31 downto 0);
    B1_data_3_in : in std_logic_vector(31 downto 0);
    B1_data_4_in : in std_logic_vector(31 downto 0);
    B1_data_5_in : in std_logic_vector(31 downto 0);
    B1_data_6_in : in std_logic_vector(31 downto 0);
    B1_data_7_in : in std_logic_vector(31 downto 0);
    B1_data_8_in : in std_logic_vector(31 downto 0);
    B2_mux_ctrl_in : in std_logic_vector(3 downto 0);
    B2_data_0_in : in std_logic_vector(0 downto 0);
    B2_data_1_in : in std_logic_vector(IMEMADDRWIDTH-1 downto 0);
    B2_data_2_in : in std_logic_vector(31 downto 0);
    B2_data_3_in : in std_logic_vector(31 downto 0);
    B2_data_4_in : in std_logic_vector(31 downto 0);
    B2_data_5_in : in std_logic_vector(31 downto 0);
    B2_data_6_in : in std_logic_vector(31 downto 0);
    B2_data_7_in : in std_logic_vector(31 downto 0);
    B5_3_mux_ctrl_in : in std_logic_vector(2 downto 0);
    B5_3_data_0_in : in std_logic_vector(31 downto 0);
    B5_3_data_1_in : in std_logic_vector(31 downto 0);
    B5_3_data_2_in : in std_logic_vector(31 downto 0);
    B5_3_data_3_in : in std_logic_vector(31 downto 0);
    B5_3_data_4_in : in std_logic_vector(31 downto 0);
    B5_3_data_5_in : in std_logic_vector(31 downto 0);
    B5_1_1_2_1_mux_ctrl_in : in std_logic_vector(2 downto 0);
    B5_1_1_2_1_data_0_in : in std_logic_vector(255 downto 0);
    B5_1_1_2_1_data_1_in : in std_logic_vector(255 downto 0);
    B5_1_1_2_1_data_2_in : in std_logic_vector(255 downto 0);
    B5_1_1_2_1_data_3_in : in std_logic_vector(255 downto 0);
    B5_1_1_2_1_data_4_in : in std_logic_vector(255 downto 0);
    B5_1_1_2_1_data_5_in : in std_logic_vector(255 downto 0);
    B5_1_1_2_1_1_mux_ctrl_in : in std_logic_vector(0 downto 0);
    B5_1_1_2_1_1_data_0_in : in std_logic_vector(255 downto 0);
    B5_1_1_2_1_1_data_1_in : in std_logic_vector(255 downto 0);
    B5_1_1_2_1_1_1_1_1_mux_ctrl_in : in std_logic_vector(1 downto 0);
    B5_1_1_2_1_1_1_1_1_data_0_in : in std_logic_vector(255 downto 0);
    B5_1_1_2_1_1_1_1_1_data_1_in : in std_logic_vector(255 downto 0);
    B5_1_1_2_1_1_1_1_1_data_2_in : in std_logic_vector(255 downto 0);
    B5_1_1_2_1_1_1_1_1_1_1_mux_ctrl_in : in std_logic_vector(1 downto 0);
    B5_1_1_2_1_1_1_1_1_1_1_data_0_in : in std_logic_vector(255 downto 0);
    B5_1_1_2_1_1_1_1_1_1_1_data_1_in : in std_logic_vector(255 downto 0);
    B5_1_1_2_1_1_1_1_1_1_1_data_2_in : in std_logic_vector(255 downto 0);
    B5_1_1_2_1_1_1_1_1_1_1_1_mux_ctrl_in : in std_logic_vector(1 downto 0);
    B5_1_1_2_1_1_1_1_1_1_1_1_data_0_in : in std_logic_vector(255 downto 0);
    B5_1_1_2_1_1_1_1_1_1_1_1_data_1_in : in std_logic_vector(255 downto 0);
    B5_1_1_2_1_1_1_1_1_1_1_1_data_2_in : in std_logic_vector(255 downto 0);
    B5_1_1_2_1_1_1_1_1_1_1_2_mux_ctrl_in : in std_logic_vector(0 downto 0);
    B5_1_1_2_1_1_1_1_1_1_1_2_data_0_in : in std_logic_vector(255 downto 0);
    B5_1_1_2_1_1_1_1_1_1_1_2_data_1_in : in std_logic_vector(255 downto 0);
    B5_1_1_2_1_1_1_1_1_1_1_3_mux_ctrl_in : in std_logic_vector(2 downto 0);
    B5_1_1_2_1_1_1_1_1_1_1_3_data_0_in : in std_logic_vector(255 downto 0);
    B5_1_1_2_1_1_1_1_1_1_1_3_data_1_in : in std_logic_vector(255 downto 0);
    B5_1_1_2_1_1_1_1_1_1_1_3_data_2_in : in std_logic_vector(255 downto 0);
    B5_1_1_2_1_1_1_1_1_1_1_3_data_3_in : in std_logic_vector(255 downto 0);
    B5_1_1_2_1_1_1_1_1_1_1_3_data_4_in : in std_logic_vector(255 downto 0);
    B5_1_1_2_1_1_1_1_1_1_1_3_data_5_in : in std_logic_vector(255 downto 0);
    B5_1_1_2_1_1_1_1_1_1_1_3_data_6_in : in std_logic_vector(255 downto 0);
    simm_B1 : in std_logic_vector(31 downto 0);
    simm_cntrl_B1 : in std_logic_vector(0 downto 0);
    simm_B2 : in std_logic_vector(31 downto 0);
    simm_cntrl_B2 : in std_logic_vector(0 downto 0);
    simm_B5_3 : in std_logic_vector(31 downto 0);
    simm_cntrl_B5_3 : in std_logic_vector(0 downto 0);
    simm_StreamSock2 : in std_logic_vector(31 downto 0);
    simm_cntrl_StreamSock2 : in std_logic_vector(0 downto 0));

end streaming_simd_tta_interconn;

architecture comb_andor of streaming_simd_tta_interconn is

  signal databus_B1 : std_logic_vector(31 downto 0);
  signal databus_B2 : std_logic_vector(31 downto 0);
  signal databus_B5_3 : std_logic_vector(31 downto 0);
  signal databus_B5_1_1_2_1 : std_logic_vector(255 downto 0);
  signal databus_B5_1_1_2_1_1 : std_logic_vector(255 downto 0);
  signal databus_B5_1_1_2_1_1_1_1_1 : std_logic_vector(255 downto 0);
  signal databus_B5_1_1_2_1_1_1_1_1_1_1 : std_logic_vector(255 downto 0);
  signal databus_B5_1_1_2_1_1_1_1_1_1_1_1 : std_logic_vector(255 downto 0);
  signal databus_B5_1_1_2_1_1_1_1_1_1_1_2 : std_logic_vector(255 downto 0);
  signal databus_B5_1_1_2_1_1_1_1_1_1_1_3 : std_logic_vector(255 downto 0);

  component streaming_simd_tta_input_mux_1 is
    generic (
      BUSW_0 : integer := 32;
      DATAW : integer := 32);
    port (
      databus0 : in std_logic_vector(BUSW_0-1 downto 0);
      data : out std_logic_vector(DATAW-1 downto 0));
  end component;

  component streaming_simd_tta_input_mux_2 is
    generic (
      BUSW_0 : integer := 32;
      BUSW_1 : integer := 32;
      DATAW : integer := 32);
    port (
      databus0 : in std_logic_vector(BUSW_0-1 downto 0);
      databus1 : in std_logic_vector(BUSW_1-1 downto 0);
      data : out std_logic_vector(DATAW-1 downto 0);
      databus_cntrl : in std_logic_vector(0 downto 0));
  end component;

  component streaming_simd_tta_input_mux_3 is
    generic (
      BUSW_0 : integer := 32;
      BUSW_1 : integer := 32;
      BUSW_2 : integer := 32;
      DATAW : integer := 32);
    port (
      databus0 : in std_logic_vector(BUSW_0-1 downto 0);
      databus1 : in std_logic_vector(BUSW_1-1 downto 0);
      databus2 : in std_logic_vector(BUSW_2-1 downto 0);
      data : out std_logic_vector(DATAW-1 downto 0);
      databus_cntrl : in std_logic_vector(1 downto 0));
  end component;

  component streaming_simd_tta_input_mux_4 is
    generic (
      BUSW_0 : integer := 32;
      BUSW_1 : integer := 32;
      BUSW_2 : integer := 32;
      BUSW_3 : integer := 32;
      DATAW : integer := 32);
    port (
      databus0 : in std_logic_vector(BUSW_0-1 downto 0);
      databus1 : in std_logic_vector(BUSW_1-1 downto 0);
      databus2 : in std_logic_vector(BUSW_2-1 downto 0);
      databus3 : in std_logic_vector(BUSW_3-1 downto 0);
      data : out std_logic_vector(DATAW-1 downto 0);
      databus_cntrl : in std_logic_vector(1 downto 0));
  end component;

  component streaming_simd_tta_input_mux_6 is
    generic (
      BUSW_0 : integer := 32;
      BUSW_1 : integer := 32;
      BUSW_2 : integer := 32;
      BUSW_3 : integer := 32;
      BUSW_4 : integer := 32;
      BUSW_5 : integer := 32;
      DATAW : integer := 32);
    port (
      databus0 : in std_logic_vector(BUSW_0-1 downto 0);
      databus1 : in std_logic_vector(BUSW_1-1 downto 0);
      databus2 : in std_logic_vector(BUSW_2-1 downto 0);
      databus3 : in std_logic_vector(BUSW_3-1 downto 0);
      databus4 : in std_logic_vector(BUSW_4-1 downto 0);
      databus5 : in std_logic_vector(BUSW_5-1 downto 0);
      data : out std_logic_vector(DATAW-1 downto 0);
      databus_cntrl : in std_logic_vector(2 downto 0));
  end component;

  component streaming_simd_tta_input_mux_7 is
    generic (
      BUSW_0 : integer := 32;
      BUSW_1 : integer := 32;
      BUSW_2 : integer := 32;
      BUSW_3 : integer := 32;
      BUSW_4 : integer := 32;
      BUSW_5 : integer := 32;
      BUSW_6 : integer := 32;
      DATAW : integer := 32);
    port (
      databus0 : in std_logic_vector(BUSW_0-1 downto 0);
      databus1 : in std_logic_vector(BUSW_1-1 downto 0);
      databus2 : in std_logic_vector(BUSW_2-1 downto 0);
      databus3 : in std_logic_vector(BUSW_3-1 downto 0);
      databus4 : in std_logic_vector(BUSW_4-1 downto 0);
      databus5 : in std_logic_vector(BUSW_5-1 downto 0);
      databus6 : in std_logic_vector(BUSW_6-1 downto 0);
      data : out std_logic_vector(DATAW-1 downto 0);
      databus_cntrl : in std_logic_vector(2 downto 0));
  end component;

  component streaming_simd_tta_input_mux_9 is
    generic (
      BUSW_0 : integer := 32;
      BUSW_1 : integer := 32;
      BUSW_2 : integer := 32;
      BUSW_3 : integer := 32;
      BUSW_4 : integer := 32;
      BUSW_5 : integer := 32;
      BUSW_6 : integer := 32;
      BUSW_7 : integer := 32;
      BUSW_8 : integer := 32;
      DATAW : integer := 32);
    port (
      databus0 : in std_logic_vector(BUSW_0-1 downto 0);
      databus1 : in std_logic_vector(BUSW_1-1 downto 0);
      databus2 : in std_logic_vector(BUSW_2-1 downto 0);
      databus3 : in std_logic_vector(BUSW_3-1 downto 0);
      databus4 : in std_logic_vector(BUSW_4-1 downto 0);
      databus5 : in std_logic_vector(BUSW_5-1 downto 0);
      databus6 : in std_logic_vector(BUSW_6-1 downto 0);
      databus7 : in std_logic_vector(BUSW_7-1 downto 0);
      databus8 : in std_logic_vector(BUSW_8-1 downto 0);
      data : out std_logic_vector(DATAW-1 downto 0);
      databus_cntrl : in std_logic_vector(3 downto 0));
  end component;

  component streaming_simd_tta_input_mux_10 is
    generic (
      BUSW_0 : integer := 32;
      BUSW_1 : integer := 32;
      BUSW_2 : integer := 32;
      BUSW_3 : integer := 32;
      BUSW_4 : integer := 32;
      BUSW_5 : integer := 32;
      BUSW_6 : integer := 32;
      BUSW_7 : integer := 32;
      BUSW_8 : integer := 32;
      BUSW_9 : integer := 32;
      DATAW : integer := 32);
    port (
      databus0 : in std_logic_vector(BUSW_0-1 downto 0);
      databus1 : in std_logic_vector(BUSW_1-1 downto 0);
      databus2 : in std_logic_vector(BUSW_2-1 downto 0);
      databus3 : in std_logic_vector(BUSW_3-1 downto 0);
      databus4 : in std_logic_vector(BUSW_4-1 downto 0);
      databus5 : in std_logic_vector(BUSW_5-1 downto 0);
      databus6 : in std_logic_vector(BUSW_6-1 downto 0);
      databus7 : in std_logic_vector(BUSW_7-1 downto 0);
      databus8 : in std_logic_vector(BUSW_8-1 downto 0);
      databus9 : in std_logic_vector(BUSW_9-1 downto 0);
      data : out std_logic_vector(DATAW-1 downto 0);
      databus_cntrl : in std_logic_vector(3 downto 0));
  end component;


begin -- comb_andor

  ALU_i1 : streaming_simd_tta_input_mux_2
    generic map (
      BUSW_0 => 32,
      BUSW_1 => 32,
      DATAW => 32)
    port map (
      databus0 => databus_B5_3,
      databus1 => databus_B1,
      data => socket_ALU_i1_data,
      databus_cntrl => socket_ALU_i1_bus_cntrl);

  ALU_i1_1_1_1_1 : streaming_simd_tta_input_mux_2
    generic map (
      BUSW_0 => 32,
      BUSW_1 => 32,
      DATAW => 15)
    port map (
      databus0 => databus_B1,
      databus1 => databus_B5_3,
      data => socket_ALU_i1_1_1_1_1_data,
      databus_cntrl => socket_ALU_i1_1_1_1_1_bus_cntrl);

  ALU_i1_1_1_1_1_1_1 : streaming_simd_tta_input_mux_2
    generic map (
      BUSW_0 => 256,
      BUSW_1 => 256,
      DATAW => 256)
    port map (
      databus0 => databus_B5_1_1_2_1_1_1_1_1_1_1_1,
      databus1 => databus_B5_1_1_2_1_1,
      data => socket_ALU_i1_1_1_1_1_1_1_data,
      databus_cntrl => socket_ALU_i1_1_1_1_1_1_1_bus_cntrl);

  ALU_i1_1_1_1_1_1_2_5 : streaming_simd_tta_input_mux_1
    generic map (
      BUSW_0 => 256,
      DATAW => 256)
    port map (
      databus0 => databus_B5_1_1_2_1_1_1_1_1_1_1_2,
      data => socket_ALU_i1_1_1_1_1_1_2_5_data);

  ALU_i1_1_1_1_1_1_2_6 : streaming_simd_tta_input_mux_1
    generic map (
      BUSW_0 => 32,
      DATAW => 32)
    port map (
      databus0 => databus_B2,
      data => socket_ALU_i1_1_1_1_1_1_2_6_data);

  ALU_i1_1_1_1_1_2 : streaming_simd_tta_input_mux_1
    generic map (
      BUSW_0 => 32,
      DATAW => 32)
    port map (
      databus0 => databus_B1,
      data => socket_ALU_i1_1_1_1_1_2_data);

  ALU_i1_1_1_1_2 : streaming_simd_tta_input_mux_2
    generic map (
      BUSW_0 => 32,
      BUSW_1 => 32,
      DATAW => 32)
    port map (
      databus0 => databus_B2,
      databus1 => databus_B1,
      data => socket_ALU_i1_1_1_1_2_data,
      databus_cntrl => socket_ALU_i1_1_1_1_2_bus_cntrl);

  ALU_i1_1_1_1_2_1 : streaming_simd_tta_input_mux_3
    generic map (
      BUSW_0 => 256,
      BUSW_1 => 256,
      BUSW_2 => 256,
      DATAW => 256)
    port map (
      databus0 => databus_B5_1_1_2_1_1_1_1_1,
      databus1 => databus_B5_1_1_2_1_1_1_1_1_1_1_1,
      databus2 => databus_B5_1_1_2_1_1_1_1_1_1_1_2,
      data => socket_ALU_i1_1_1_1_2_1_data,
      databus_cntrl => socket_ALU_i1_1_1_1_2_1_bus_cntrl);

  ALU_i1_1_1_1_3 : streaming_simd_tta_input_mux_1
    generic map (
      BUSW_0 => 32,
      DATAW => 12)
    port map (
      databus0 => databus_B2,
      data => socket_ALU_i1_1_1_1_3_data);

  ALU_i1_4 : streaming_simd_tta_input_mux_1
    generic map (
      BUSW_0 => 32,
      DATAW => 32)
    port map (
      databus0 => databus_B2,
      data => socket_ALU_i1_4_data);

  ALU_i2 : streaming_simd_tta_input_mux_2
    generic map (
      BUSW_0 => 32,
      BUSW_1 => 32,
      DATAW => 32)
    port map (
      databus0 => databus_B2,
      databus1 => databus_B1,
      data => socket_ALU_i2_data,
      databus_cntrl => socket_ALU_i2_bus_cntrl);

  ALU_i2_1 : streaming_simd_tta_input_mux_2
    generic map (
      BUSW_0 => 32,
      BUSW_1 => 32,
      DATAW => 32)
    port map (
      databus0 => databus_B1,
      databus1 => databus_B5_3,
      data => socket_ALU_i2_1_data,
      databus_cntrl => socket_ALU_i2_1_bus_cntrl);

  ALU_i2_2 : streaming_simd_tta_input_mux_2
    generic map (
      BUSW_0 => 32,
      BUSW_1 => 32,
      DATAW => 32)
    port map (
      databus0 => databus_B2,
      databus1 => databus_B5_3,
      data => socket_ALU_i2_2_data,
      databus_cntrl => socket_ALU_i2_2_bus_cntrl);

  ALU_o1_1_1_2_1 : streaming_simd_tta_input_mux_4
    generic map (
      BUSW_0 => 256,
      BUSW_1 => 256,
      BUSW_2 => 256,
      BUSW_3 => 256,
      DATAW => 256)
    port map (
      databus0 => databus_B5_1_1_2_1,
      databus1 => databus_B5_1_1_2_1_1_1_1_1_1_1_1,
      databus2 => databus_B5_1_1_2_1_1_1_1_1,
      databus3 => databus_B5_1_1_2_1_1_1_1_1_1_1_3,
      data => socket_ALU_o1_1_1_2_1_data,
      databus_cntrl => socket_ALU_o1_1_1_2_1_bus_cntrl);

  ALU_o1_1_1_2_1_1 : streaming_simd_tta_input_mux_2
    generic map (
      BUSW_0 => 256,
      BUSW_1 => 256,
      DATAW => 256)
    port map (
      databus0 => databus_B5_1_1_2_1_1,
      databus1 => databus_B5_1_1_2_1_1_1_1_1_1_1_2,
      data => socket_ALU_o1_1_1_2_1_1_data,
      databus_cntrl => socket_ALU_o1_1_1_2_1_1_bus_cntrl);

  ALU_o1_1_1_2_1_1_1 : streaming_simd_tta_input_mux_2
    generic map (
      BUSW_0 => 256,
      BUSW_1 => 256,
      DATAW => 256)
    port map (
      databus0 => databus_B5_1_1_2_1_1,
      databus1 => databus_B5_1_1_2_1_1_1_1_1_1_1_2,
      data => socket_ALU_o1_1_1_2_1_1_1_data,
      databus_cntrl => socket_ALU_o1_1_1_2_1_1_1_bus_cntrl);

  ALU_o1_1_1_2_1_2 : streaming_simd_tta_input_mux_2
    generic map (
      BUSW_0 => 256,
      BUSW_1 => 256,
      DATAW => 256)
    port map (
      databus0 => databus_B5_1_1_2_1,
      databus1 => databus_B5_1_1_2_1_1_1_1_1_1_1_1,
      data => socket_ALU_o1_1_1_2_1_2_data,
      databus_cntrl => socket_ALU_o1_1_1_2_1_2_bus_cntrl);

  ALU_o1_1_1_2_1_2_1 : streaming_simd_tta_input_mux_2
    generic map (
      BUSW_0 => 256,
      BUSW_1 => 256,
      DATAW => 256)
    port map (
      databus0 => databus_B5_1_1_2_1,
      databus1 => databus_B5_1_1_2_1_1_1_1_1_1_1_1,
      data => socket_ALU_o1_1_1_2_1_2_1_data,
      databus_cntrl => socket_ALU_o1_1_1_2_1_2_1_bus_cntrl);

  ALU_o1_1_1_2_1_3 : streaming_simd_tta_input_mux_4
    generic map (
      BUSW_0 => 256,
      BUSW_1 => 256,
      BUSW_2 => 256,
      BUSW_3 => 256,
      DATAW => 256)
    port map (
      databus0 => databus_B5_1_1_2_1,
      databus1 => databus_B5_1_1_2_1_1_1_1_1_1_1_1,
      databus2 => databus_B5_1_1_2_1_1_1_1_1,
      databus3 => databus_B5_1_1_2_1_1_1_1_1_1_1_3,
      data => socket_ALU_o1_1_1_2_1_3_data,
      databus_cntrl => socket_ALU_o1_1_1_2_1_3_bus_cntrl);

  StreamSock1 : streaming_simd_tta_input_mux_3
    generic map (
      BUSW_0 => 32,
      BUSW_1 => 32,
      BUSW_2 => 32,
      DATAW => 32)
    port map (
      databus0 => databus_B1,
      databus1 => databus_B2,
      databus2 => databus_B5_3,
      data => socket_StreamSock1_data,
      databus_cntrl => socket_StreamSock1_bus_cntrl);

  StreamSock2 : streaming_simd_tta_input_mux_3
    generic map (
      BUSW_0 => 32,
      BUSW_1 => 32,
      BUSW_2 => 32,
      DATAW => 32)
    port map (
      databus0 => databus_B2,
      databus1 => databus_B1,
      databus2 => databus_B5_3,
      data => socket_StreamSock2_data,
      databus_cntrl => socket_StreamSock2_bus_cntrl);

  StreamSock2_1 : streaming_simd_tta_input_mux_3
    generic map (
      BUSW_0 => 32,
      BUSW_1 => 32,
      BUSW_2 => 32,
      DATAW => 32)
    port map (
      databus0 => databus_B2,
      databus1 => databus_B1,
      databus2 => databus_B5_3,
      data => socket_StreamSock2_1_data,
      databus_cntrl => socket_StreamSock2_1_bus_cntrl);

  StreamSockV3 : streaming_simd_tta_input_mux_3
    generic map (
      BUSW_0 => 256,
      BUSW_1 => 256,
      BUSW_2 => 256,
      DATAW => 256)
    port map (
      databus0 => databus_B5_1_1_2_1,
      databus1 => databus_B5_1_1_2_1_1_1_1_1_1_1,
      databus2 => databus_B5_1_1_2_1_1_1_1_1_1_1_3,
      data => socket_StreamSockV3_data,
      databus_cntrl => socket_StreamSockV3_bus_cntrl);

  StreamSockV3_1 : streaming_simd_tta_input_mux_3
    generic map (
      BUSW_0 => 256,
      BUSW_1 => 256,
      BUSW_2 => 256,
      DATAW => 256)
    port map (
      databus0 => databus_B5_1_1_2_1,
      databus1 => databus_B5_1_1_2_1_1_1_1_1_1_1,
      databus2 => databus_B5_1_1_2_1_1_1_1_1_1_1_3,
      data => socket_StreamSockV3_1_data,
      databus_cntrl => socket_StreamSockV3_1_bus_cntrl);

  StreamSockV3_2 : streaming_simd_tta_input_mux_3
    generic map (
      BUSW_0 => 256,
      BUSW_1 => 256,
      BUSW_2 => 256,
      DATAW => 256)
    port map (
      databus0 => databus_B5_1_1_2_1,
      databus1 => databus_B5_1_1_2_1_1_1_1_1_1_1,
      databus2 => databus_B5_1_1_2_1_1_1_1_1_1_1_3,
      data => socket_StreamSockV3_2_data,
      databus_cntrl => socket_StreamSockV3_2_bus_cntrl);

  W1_32 : streaming_simd_tta_input_mux_1
    generic map (
      BUSW_0 => 32,
      DATAW => 32)
    port map (
      databus0 => databus_B1,
      data => socket_W1_32_data);

  W1_32_1 : streaming_simd_tta_input_mux_2
    generic map (
      BUSW_0 => 256,
      BUSW_1 => 256,
      DATAW => 256)
    port map (
      databus0 => databus_B5_1_1_2_1_1_1_1_1_1_1_3,
      databus1 => databus_B5_1_1_2_1,
      data => socket_W1_32_1_data,
      databus_cntrl => socket_W1_32_1_bus_cntrl);

  bool_i1 : streaming_simd_tta_input_mux_2
    generic map (
      BUSW_0 => 32,
      BUSW_1 => 32,
      DATAW => 1)
    port map (
      databus0 => databus_B1,
      databus1 => databus_B2,
      data => socket_bool_i1_data,
      databus_cntrl => socket_bool_i1_bus_cntrl);

  gcu_i1 : streaming_simd_tta_input_mux_2
    generic map (
      BUSW_0 => 32,
      BUSW_1 => 32,
      DATAW => IMEMADDRWIDTH)
    port map (
      databus0 => databus_B1,
      databus1 => databus_B2,
      data => socket_gcu_i1_data,
      databus_cntrl => socket_gcu_i1_bus_cntrl);

  gcu_i2 : streaming_simd_tta_input_mux_2
    generic map (
      BUSW_0 => 32,
      BUSW_1 => 32,
      DATAW => IMEMADDRWIDTH)
    port map (
      databus0 => databus_B1,
      databus1 => databus_B2,
      data => socket_gcu_i2_data,
      databus_cntrl => socket_gcu_i2_bus_cntrl);

  B1_bus_mux_inst : streaming_simd_tta_input_mux_10
    generic map (
      BUSW_0 => 1,
      BUSW_1 => IMEMADDRWIDTH,
      BUSW_2 => 32,
      BUSW_3 => 32,
      BUSW_4 => 32,
      BUSW_5 => 32,
      BUSW_6 => 32,
      BUSW_7 => 32,
      BUSW_8 => 32,
      BUSW_9 => 32,
      DATAW => 32)
    port map (
      databus0 => B1_data_0_in,
      databus1 => B1_data_1_in,
      databus2 => B1_data_2_in,
      databus3 => B1_data_3_in,
      databus4 => B1_data_4_in,
      databus5 => B1_data_5_in,
      databus6 => B1_data_6_in,
      databus7 => B1_data_7_in,
      databus8 => B1_data_8_in,
      databus9 => simm_B1,
      data => databus_B1,
      databus_cntrl => B1_mux_ctrl_in);

  B2_bus_mux_inst : streaming_simd_tta_input_mux_9
    generic map (
      BUSW_0 => 1,
      BUSW_1 => IMEMADDRWIDTH,
      BUSW_2 => 32,
      BUSW_3 => 32,
      BUSW_4 => 32,
      BUSW_5 => 32,
      BUSW_6 => 32,
      BUSW_7 => 32,
      BUSW_8 => 32,
      DATAW => 32)
    port map (
      databus0 => B2_data_0_in,
      databus1 => B2_data_1_in,
      databus2 => B2_data_2_in,
      databus3 => B2_data_3_in,
      databus4 => B2_data_4_in,
      databus5 => B2_data_5_in,
      databus6 => B2_data_6_in,
      databus7 => B2_data_7_in,
      databus8 => simm_B2,
      data => databus_B2,
      databus_cntrl => B2_mux_ctrl_in);

  B5_3_bus_mux_inst : streaming_simd_tta_input_mux_7
    generic map (
      BUSW_0 => 32,
      BUSW_1 => 32,
      BUSW_2 => 32,
      BUSW_3 => 32,
      BUSW_4 => 32,
      BUSW_5 => 32,
      BUSW_6 => 32,
      DATAW => 32)
    port map (
      databus0 => B5_3_data_0_in,
      databus1 => B5_3_data_1_in,
      databus2 => B5_3_data_2_in,
      databus3 => B5_3_data_3_in,
      databus4 => B5_3_data_4_in,
      databus5 => B5_3_data_5_in,
      databus6 => simm_B5_3,
      data => databus_B5_3,
      databus_cntrl => B5_3_mux_ctrl_in);

  B5_1_1_2_1_bus_mux_inst : streaming_simd_tta_input_mux_6
    generic map (
      BUSW_0 => 256,
      BUSW_1 => 256,
      BUSW_2 => 256,
      BUSW_3 => 256,
      BUSW_4 => 256,
      BUSW_5 => 256,
      DATAW => 256)
    port map (
      databus0 => B5_1_1_2_1_data_0_in,
      databus1 => B5_1_1_2_1_data_1_in,
      databus2 => B5_1_1_2_1_data_2_in,
      databus3 => B5_1_1_2_1_data_3_in,
      databus4 => B5_1_1_2_1_data_4_in,
      databus5 => B5_1_1_2_1_data_5_in,
      data => databus_B5_1_1_2_1,
      databus_cntrl => B5_1_1_2_1_mux_ctrl_in);

  B5_1_1_2_1_1_bus_mux_inst : streaming_simd_tta_input_mux_2
    generic map (
      BUSW_0 => 256,
      BUSW_1 => 256,
      DATAW => 256)
    port map (
      databus0 => B5_1_1_2_1_1_data_0_in,
      databus1 => B5_1_1_2_1_1_data_1_in,
      data => databus_B5_1_1_2_1_1,
      databus_cntrl => B5_1_1_2_1_1_mux_ctrl_in);

  B5_1_1_2_1_1_1_1_1_bus_mux_inst : streaming_simd_tta_input_mux_3
    generic map (
      BUSW_0 => 256,
      BUSW_1 => 256,
      BUSW_2 => 256,
      DATAW => 256)
    port map (
      databus0 => B5_1_1_2_1_1_1_1_1_data_0_in,
      databus1 => B5_1_1_2_1_1_1_1_1_data_1_in,
      databus2 => B5_1_1_2_1_1_1_1_1_data_2_in,
      data => databus_B5_1_1_2_1_1_1_1_1,
      databus_cntrl => B5_1_1_2_1_1_1_1_1_mux_ctrl_in);

  B5_1_1_2_1_1_1_1_1_1_1_bus_mux_inst : streaming_simd_tta_input_mux_3
    generic map (
      BUSW_0 => 256,
      BUSW_1 => 256,
      BUSW_2 => 256,
      DATAW => 256)
    port map (
      databus0 => B5_1_1_2_1_1_1_1_1_1_1_data_0_in,
      databus1 => B5_1_1_2_1_1_1_1_1_1_1_data_1_in,
      databus2 => B5_1_1_2_1_1_1_1_1_1_1_data_2_in,
      data => databus_B5_1_1_2_1_1_1_1_1_1_1,
      databus_cntrl => B5_1_1_2_1_1_1_1_1_1_1_mux_ctrl_in);

  B5_1_1_2_1_1_1_1_1_1_1_1_bus_mux_inst : streaming_simd_tta_input_mux_3
    generic map (
      BUSW_0 => 256,
      BUSW_1 => 256,
      BUSW_2 => 256,
      DATAW => 256)
    port map (
      databus0 => B5_1_1_2_1_1_1_1_1_1_1_1_data_0_in,
      databus1 => B5_1_1_2_1_1_1_1_1_1_1_1_data_1_in,
      databus2 => B5_1_1_2_1_1_1_1_1_1_1_1_data_2_in,
      data => databus_B5_1_1_2_1_1_1_1_1_1_1_1,
      databus_cntrl => B5_1_1_2_1_1_1_1_1_1_1_1_mux_ctrl_in);

  B5_1_1_2_1_1_1_1_1_1_1_2_bus_mux_inst : streaming_simd_tta_input_mux_2
    generic map (
      BUSW_0 => 256,
      BUSW_1 => 256,
      DATAW => 256)
    port map (
      databus0 => B5_1_1_2_1_1_1_1_1_1_1_2_data_0_in,
      databus1 => B5_1_1_2_1_1_1_1_1_1_1_2_data_1_in,
      data => databus_B5_1_1_2_1_1_1_1_1_1_1_2,
      databus_cntrl => B5_1_1_2_1_1_1_1_1_1_1_2_mux_ctrl_in);

  B5_1_1_2_1_1_1_1_1_1_1_3_bus_mux_inst : streaming_simd_tta_input_mux_7
    generic map (
      BUSW_0 => 256,
      BUSW_1 => 256,
      BUSW_2 => 256,
      BUSW_3 => 256,
      BUSW_4 => 256,
      BUSW_5 => 256,
      BUSW_6 => 256,
      DATAW => 256)
    port map (
      databus0 => B5_1_1_2_1_1_1_1_1_1_1_3_data_0_in,
      databus1 => B5_1_1_2_1_1_1_1_1_1_1_3_data_1_in,
      databus2 => B5_1_1_2_1_1_1_1_1_1_1_3_data_2_in,
      databus3 => B5_1_1_2_1_1_1_1_1_1_1_3_data_3_in,
      databus4 => B5_1_1_2_1_1_1_1_1_1_1_3_data_4_in,
      databus5 => B5_1_1_2_1_1_1_1_1_1_1_3_data_5_in,
      databus6 => B5_1_1_2_1_1_1_1_1_1_1_3_data_6_in,
      data => databus_B5_1_1_2_1_1_1_1_1_1_1_3,
      databus_cntrl => B5_1_1_2_1_1_1_1_1_1_1_3_mux_ctrl_in);


end comb_andor;
