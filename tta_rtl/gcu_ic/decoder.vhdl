library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use work.streaming_simd_tta_globals.all;
use work.streaming_simd_tta_gcu_opcodes.all;
use work.tce_util.all;

entity streaming_simd_tta_decoder is

  port (
    instructionword : in std_logic_vector(INSTRUCTIONWIDTH-1 downto 0);
    pc_load : out std_logic;
    ra_load : out std_logic;
    pc_opcode : out std_logic_vector(0 downto 0);
    lock : in std_logic;
    lock_r : out std_logic;
    clk : in std_logic;
    rstx : in std_logic;
    locked : out std_logic;
    simm_B1 : out std_logic_vector(31 downto 0);
    simm_B2 : out std_logic_vector(31 downto 0);
    simm_B5_3 : out std_logic_vector(31 downto 0);
    simm_StreamSock2 : out std_logic_vector(31 downto 0);
    socket_bool_i1_bus_cntrl : out std_logic_vector(0 downto 0);
    socket_gcu_i1_bus_cntrl : out std_logic_vector(0 downto 0);
    socket_gcu_i2_bus_cntrl : out std_logic_vector(0 downto 0);
    socket_ALU_i1_bus_cntrl : out std_logic_vector(0 downto 0);
    socket_ALU_i2_bus_cntrl : out std_logic_vector(0 downto 0);
    socket_ALU_i1_1_1_1_1_bus_cntrl : out std_logic_vector(0 downto 0);
    socket_ALU_o1_1_1_2_1_bus_cntrl : out std_logic_vector(1 downto 0);
    socket_ALU_o1_1_1_2_1_1_bus_cntrl : out std_logic_vector(0 downto 0);
    socket_ALU_o1_1_1_2_1_2_bus_cntrl : out std_logic_vector(0 downto 0);
    socket_W1_32_1_bus_cntrl : out std_logic_vector(0 downto 0);
    socket_ALU_i1_1_1_1_1_1_1_bus_cntrl : out std_logic_vector(0 downto 0);
    socket_ALU_i1_1_1_1_2_1_bus_cntrl : out std_logic_vector(1 downto 0);
    socket_ALU_i2_1_bus_cntrl : out std_logic_vector(0 downto 0);
    socket_ALU_i2_2_bus_cntrl : out std_logic_vector(0 downto 0);
    socket_ALU_i1_1_1_1_2_bus_cntrl : out std_logic_vector(0 downto 0);
    socket_StreamSock1_bus_cntrl : out std_logic_vector(1 downto 0);
    socket_StreamSock2_bus_cntrl : out std_logic_vector(1 downto 0);
    socket_StreamSockV3_bus_cntrl : out std_logic_vector(1 downto 0);
    socket_StreamSockV3_1_bus_cntrl : out std_logic_vector(1 downto 0);
    socket_StreamSockV3_2_bus_cntrl : out std_logic_vector(1 downto 0);
    socket_StreamSock2_1_bus_cntrl : out std_logic_vector(1 downto 0);
    socket_ALU_o1_1_1_2_1_2_1_bus_cntrl : out std_logic_vector(0 downto 0);
    socket_ALU_o1_1_1_2_1_1_1_bus_cntrl : out std_logic_vector(0 downto 0);
    socket_ALU_o1_1_1_2_1_3_bus_cntrl : out std_logic_vector(1 downto 0);
    B1_src_sel : out std_logic_vector(3 downto 0);
    B2_src_sel : out std_logic_vector(3 downto 0);
    B5_3_src_sel : out std_logic_vector(2 downto 0);
    B5_1_1_2_1_src_sel : out std_logic_vector(2 downto 0);
    B5_1_1_2_1_1_src_sel : out std_logic_vector(0 downto 0);
    B5_1_1_2_1_1_1_1_1_src_sel : out std_logic_vector(1 downto 0);
    B5_1_1_2_1_1_1_1_1_1_1_src_sel : out std_logic_vector(1 downto 0);
    B5_1_1_2_1_1_1_1_1_1_1_1_src_sel : out std_logic_vector(1 downto 0);
    B5_1_1_2_1_1_1_1_1_1_1_2_src_sel : out std_logic_vector(0 downto 0);
    B5_1_1_2_1_1_1_1_1_1_1_3_src_sel : out std_logic_vector(2 downto 0);
    fu_ALU_0_in1t_load : out std_logic;
    fu_ALU_0_in2_load : out std_logic;
    fu_ALU_0_opc : out std_logic_vector(3 downto 0);
    fu_VALU_int0_load : out std_logic;
    fu_VALU_in2_load : out std_logic;
    fu_VALU_in1_load : out std_logic;
    fu_VALU_opc : out std_logic_vector(2 downto 0);
    fu_VBCAST_in1t_load : out std_logic;
    fu_VSHUFFLE_in1t_load : out std_logic;
    fu_VSHUFFLE_in2_load : out std_logic;
    fu_VSHUFFLE_opc : out std_logic_vector(1 downto 0);
    fu_VEXTRACT_in1t_load : out std_logic;
    fu_VEXTRACT_in2_load : out std_logic;
    fu_ALU_1_in1t_load : out std_logic;
    fu_ALU_1_in2_load : out std_logic;
    fu_ALU_1_opc : out std_logic_vector(3 downto 0);
    fu_PARAM_LSU_in1t_load : out std_logic;
    fu_PARAM_LSU_in2_load : out std_logic;
    fu_PARAM_LSU_opc : out std_logic_vector(2 downto 0);
    fu_DATA_LSU_in1t_load : out std_logic;
    fu_DATA_LSU_in2_load : out std_logic;
    fu_DATA_LSU_opc : out std_logic_vector(2 downto 0);
    fu_Streamin1_t1_load : out std_logic;
    fu_Streamin2_t1_load : out std_logic;
    fu_Streamout_t1_load : out std_logic;
    fu_Streamout2_t1_load : out std_logic;
    fu_Streamin3_t1_load : out std_logic;
    fu_Streamout3_t1_load : out std_logic;
    fu_VALU_1_int0_load : out std_logic;
    fu_VALU_1_in2_load : out std_logic;
    fu_VALU_1_in1_load : out std_logic;
    fu_VALU_1_opc : out std_logic_vector(2 downto 0);
    rf_BOOL_wr_load : out std_logic;
    rf_BOOL_wr_opc : out std_logic_vector(0 downto 0);
    rf_BOOL_rd_load : out std_logic;
    rf_BOOL_rd_opc : out std_logic_vector(0 downto 0);
    rf_RF_32_0_R1_32_load : out std_logic;
    rf_RF_32_0_R1_32_opc : out std_logic_vector(4 downto 0);
    rf_RF_32_0_W1_32_load : out std_logic;
    rf_RF_32_0_W1_32_opc : out std_logic_vector(4 downto 0);
    rf_RF_32_0_R2_load : out std_logic;
    rf_RF_32_0_R2_opc : out std_logic_vector(4 downto 0);
    rf_RF_256_0_R1_32_load : out std_logic;
    rf_RF_256_0_R1_32_opc : out std_logic_vector(4 downto 0);
    rf_RF_256_0_W1_32_load : out std_logic;
    rf_RF_256_0_W1_32_opc : out std_logic_vector(4 downto 0);
    rf_RF_256_0_R2_load : out std_logic;
    rf_RF_256_0_R2_opc : out std_logic_vector(4 downto 0);
    iu_IU_1x32_r0_read_load : out std_logic;
    iu_IU_1x32_r0_read_opc : out std_logic_vector(0 downto 0);
    iu_IU_1x32_write : out std_logic_vector(31 downto 0);
    iu_IU_1x32_write_load : out std_logic;
    iu_IU_1x32_write_opc : out std_logic_vector(0 downto 0);
    rf_guard_BOOL_0 : in std_logic;
    rf_guard_BOOL_1 : in std_logic;
    lock_req : in std_logic_vector(15 downto 0);
    glock : out std_logic_vector(19 downto 0);
    db_tta_nreset : in std_logic);

end streaming_simd_tta_decoder;

architecture rtl_andor of streaming_simd_tta_decoder is

  -- signals for source, destination and guard fields
  signal move_B1 : std_logic_vector(22 downto 0);
  signal src_B1 : std_logic_vector(12 downto 0);
  signal dst_B1 : std_logic_vector(6 downto 0);
  signal grd_B1 : std_logic_vector(2 downto 0);
  signal move_B2 : std_logic_vector(17 downto 0);
  signal src_B2 : std_logic_vector(8 downto 0);
  signal dst_B2 : std_logic_vector(5 downto 0);
  signal grd_B2 : std_logic_vector(2 downto 0);
  signal move_B5_3 : std_logic_vector(17 downto 0);
  signal src_B5_3 : std_logic_vector(8 downto 0);
  signal dst_B5_3 : std_logic_vector(5 downto 0);
  signal grd_B5_3 : std_logic_vector(2 downto 0);
  signal move_B5_1_1_2_1 : std_logic_vector(14 downto 0);
  signal src_B5_1_1_2_1 : std_logic_vector(5 downto 0);
  signal dst_B5_1_1_2_1 : std_logic_vector(5 downto 0);
  signal grd_B5_1_1_2_1 : std_logic_vector(2 downto 0);
  signal move_B5_1_1_2_1_1 : std_logic_vector(6 downto 0);
  signal src_B5_1_1_2_1_1 : std_logic_vector(0 downto 0);
  signal dst_B5_1_1_2_1_1 : std_logic_vector(2 downto 0);
  signal grd_B5_1_1_2_1_1 : std_logic_vector(2 downto 0);
  signal move_B5_1_1_2_1_1_1_1_1 : std_logic_vector(9 downto 0);
  signal src_B5_1_1_2_1_1_1_1_1 : std_logic_vector(1 downto 0);
  signal dst_B5_1_1_2_1_1_1_1_1 : std_logic_vector(4 downto 0);
  signal grd_B5_1_1_2_1_1_1_1_1 : std_logic_vector(2 downto 0);
  signal move_B5_1_1_2_1_1_1_1_1_1_1 : std_logic_vector(6 downto 0);
  signal src_B5_1_1_2_1_1_1_1_1_1_1 : std_logic_vector(1 downto 0);
  signal dst_B5_1_1_2_1_1_1_1_1_1_1 : std_logic_vector(1 downto 0);
  signal grd_B5_1_1_2_1_1_1_1_1_1_1 : std_logic_vector(2 downto 0);
  signal move_B5_1_1_2_1_1_1_1_1_1_1_1 : std_logic_vector(13 downto 0);
  signal src_B5_1_1_2_1_1_1_1_1_1_1_1 : std_logic_vector(5 downto 0);
  signal dst_B5_1_1_2_1_1_1_1_1_1_1_1 : std_logic_vector(4 downto 0);
  signal grd_B5_1_1_2_1_1_1_1_1_1_1_1 : std_logic_vector(2 downto 0);
  signal move_B5_1_1_2_1_1_1_1_1_1_1_2 : std_logic_vector(10 downto 0);
  signal src_B5_1_1_2_1_1_1_1_1_1_1_2 : std_logic_vector(5 downto 0);
  signal dst_B5_1_1_2_1_1_1_1_1_1_1_2 : std_logic_vector(1 downto 0);
  signal grd_B5_1_1_2_1_1_1_1_1_1_1_2 : std_logic_vector(2 downto 0);
  signal move_B5_1_1_2_1_1_1_1_1_1_1_3 : std_logic_vector(14 downto 0);
  signal src_B5_1_1_2_1_1_1_1_1_1_1_3 : std_logic_vector(5 downto 0);
  signal dst_B5_1_1_2_1_1_1_1_1_1_1_3 : std_logic_vector(5 downto 0);
  signal grd_B5_1_1_2_1_1_1_1_1_1_1_3 : std_logic_vector(2 downto 0);
  signal move_StreamSock2 : std_logic_vector(32 downto 0);
  signal src_StreamSock2 : std_logic_vector(31 downto 0);

  -- signals for dedicated immediate slots

  -- signal for long immediate tag
  signal limm_tag : std_logic_vector(0 downto 0);

  -- squash signals
  signal squash_B1 : std_logic;
  signal squash_B2 : std_logic;
  signal squash_B5_3 : std_logic;
  signal squash_B5_1_1_2_1 : std_logic;
  signal squash_B5_1_1_2_1_1 : std_logic;
  signal squash_B5_1_1_2_1_1_1_1_1 : std_logic;
  signal squash_B5_1_1_2_1_1_1_1_1_1_1 : std_logic;
  signal squash_B5_1_1_2_1_1_1_1_1_1_1_1 : std_logic;
  signal squash_B5_1_1_2_1_1_1_1_1_1_1_2 : std_logic;
  signal squash_B5_1_1_2_1_1_1_1_1_1_1_3 : std_logic;
  signal squash_StreamSock2 : std_logic;

  -- socket control signals
  signal socket_bool_i1_bus_cntrl_reg : std_logic_vector(0 downto 0);
  signal socket_bool_o1_bus_cntrl_reg : std_logic_vector(1 downto 0);
  signal socket_gcu_i1_bus_cntrl_reg : std_logic_vector(0 downto 0);
  signal socket_gcu_i2_bus_cntrl_reg : std_logic_vector(0 downto 0);
  signal socket_gcu_o1_bus_cntrl_reg : std_logic_vector(1 downto 0);
  signal socket_ALU_i1_bus_cntrl_reg : std_logic_vector(0 downto 0);
  signal socket_ALU_i2_bus_cntrl_reg : std_logic_vector(0 downto 0);
  signal socket_ALU_o1_bus_cntrl_reg : std_logic_vector(2 downto 0);
  signal socket_IMM_rd_bus_cntrl_reg : std_logic_vector(2 downto 0);
  signal socket_R1_32_bus_cntrl_reg : std_logic_vector(1 downto 0);
  signal socket_ALU_o1_1_bus_cntrl_reg : std_logic_vector(1 downto 0);
  signal socket_ALU_i1_1_1_1_1_bus_cntrl_reg : std_logic_vector(0 downto 0);
  signal socket_ALU_o1_1_1_1_bus_cntrl_reg : std_logic_vector(2 downto 0);
  signal socket_ALU_o1_1_1_3_bus_cntrl_reg : std_logic_vector(2 downto 0);
  signal socket_ALU_o1_1_1_2_1_bus_cntrl_reg : std_logic_vector(1 downto 0);
  signal socket_ALU_o1_1_1_2_1_1_bus_cntrl_reg : std_logic_vector(0 downto 0);
  signal socket_ALU_o1_1_1_2_1_2_bus_cntrl_reg : std_logic_vector(0 downto 0);
  signal socket_W1_32_1_bus_cntrl_reg : std_logic_vector(0 downto 0);
  signal socket_R1_32_1_bus_cntrl_reg : std_logic_vector(1 downto 0);
  signal socket_ALU_i1_1_1_1_1_1_1_bus_cntrl_reg : std_logic_vector(0 downto 0);
  signal socket_ALU_i1_1_1_1_2_1_bus_cntrl_reg : std_logic_vector(1 downto 0);
  signal socket_ALU_o1_1_2_bus_cntrl_reg : std_logic_vector(2 downto 0);
  signal socket_ALU_o1_1_2_1_bus_cntrl_reg : std_logic_vector(0 downto 0);
  signal socket_ALU_o1_1_1_1_2_bus_cntrl_reg : std_logic_vector(1 downto 0);
  signal socket_ALU_i2_1_bus_cntrl_reg : std_logic_vector(0 downto 0);
  signal socket_ALU_i2_2_bus_cntrl_reg : std_logic_vector(0 downto 0);
  signal socket_ALU_o1_2_bus_cntrl_reg : std_logic_vector(2 downto 0);
  signal socket_R1_32_2_bus_cntrl_reg : std_logic_vector(1 downto 0);
  signal socket_R1_32_1_2_bus_cntrl_reg : std_logic_vector(1 downto 0);
  signal socket_ALU_i1_1_1_1_2_bus_cntrl_reg : std_logic_vector(0 downto 0);
  signal socket_StreamSock1_bus_cntrl_reg : std_logic_vector(1 downto 0);
  signal socket_StreamSock2_bus_cntrl_reg : std_logic_vector(1 downto 0);
  signal socket_StreamSockV1_bus_cntrl_reg : std_logic_vector(2 downto 0);
  signal socket_StreamSockV2_bus_cntrl_reg : std_logic_vector(3 downto 0);
  signal socket_StreamSockV3_bus_cntrl_reg : std_logic_vector(1 downto 0);
  signal socket_StreamSockV3_1_bus_cntrl_reg : std_logic_vector(1 downto 0);
  signal socket_StreamSockV3_2_bus_cntrl_reg : std_logic_vector(1 downto 0);
  signal socket_StreamSockV2_1_bus_cntrl_reg : std_logic_vector(3 downto 0);
  signal socket_StreamSock2_1_bus_cntrl_reg : std_logic_vector(1 downto 0);
  signal socket_ALU_o1_1_1_2_1_2_1_bus_cntrl_reg : std_logic_vector(0 downto 0);
  signal socket_ALU_o1_1_1_2_1_1_1_bus_cntrl_reg : std_logic_vector(0 downto 0);
  signal socket_ALU_o1_1_1_2_1_3_bus_cntrl_reg : std_logic_vector(1 downto 0);
  signal socket_ALU_o1_1_1_3_1_bus_cntrl_reg : std_logic_vector(2 downto 0);
  signal simm_B1_reg : std_logic_vector(31 downto 0);
  signal B1_src_sel_reg : std_logic_vector(3 downto 0);
  signal simm_B2_reg : std_logic_vector(31 downto 0);
  signal B2_src_sel_reg : std_logic_vector(3 downto 0);
  signal simm_B5_3_reg : std_logic_vector(31 downto 0);
  signal B5_3_src_sel_reg : std_logic_vector(2 downto 0);
  signal B5_1_1_2_1_src_sel_reg : std_logic_vector(2 downto 0);
  signal B5_1_1_2_1_1_src_sel_reg : std_logic_vector(0 downto 0);
  signal B5_1_1_2_1_1_1_1_1_src_sel_reg : std_logic_vector(1 downto 0);
  signal B5_1_1_2_1_1_1_1_1_1_1_src_sel_reg : std_logic_vector(1 downto 0);
  signal B5_1_1_2_1_1_1_1_1_1_1_1_src_sel_reg : std_logic_vector(1 downto 0);
  signal B5_1_1_2_1_1_1_1_1_1_1_2_src_sel_reg : std_logic_vector(0 downto 0);
  signal B5_1_1_2_1_1_1_1_1_1_1_3_src_sel_reg : std_logic_vector(2 downto 0);
  signal simm_StreamSock2_reg : std_logic_vector(31 downto 0);

  -- FU control signals
  signal fu_ALU_0_in1t_load_reg : std_logic;
  signal fu_ALU_0_in2_load_reg : std_logic;
  signal fu_ALU_0_opc_reg : std_logic_vector(3 downto 0);
  signal fu_VALU_int0_load_reg : std_logic;
  signal fu_VALU_in2_load_reg : std_logic;
  signal fu_VALU_in1_load_reg : std_logic;
  signal fu_VALU_opc_reg : std_logic_vector(2 downto 0);
  signal fu_VBCAST_in1t_load_reg : std_logic;
  signal fu_VSHUFFLE_in1t_load_reg : std_logic;
  signal fu_VSHUFFLE_in2_load_reg : std_logic;
  signal fu_VSHUFFLE_opc_reg : std_logic_vector(1 downto 0);
  signal fu_VEXTRACT_in1t_load_reg : std_logic;
  signal fu_VEXTRACT_in2_load_reg : std_logic;
  signal fu_ALU_1_in1t_load_reg : std_logic;
  signal fu_ALU_1_in2_load_reg : std_logic;
  signal fu_ALU_1_opc_reg : std_logic_vector(3 downto 0);
  signal fu_PARAM_LSU_in1t_load_reg : std_logic;
  signal fu_PARAM_LSU_in2_load_reg : std_logic;
  signal fu_PARAM_LSU_opc_reg : std_logic_vector(2 downto 0);
  signal fu_DATA_LSU_in1t_load_reg : std_logic;
  signal fu_DATA_LSU_in2_load_reg : std_logic;
  signal fu_DATA_LSU_opc_reg : std_logic_vector(2 downto 0);
  signal fu_Streamin1_t1_load_reg : std_logic;
  signal fu_Streamin2_t1_load_reg : std_logic;
  signal fu_Streamout_t1_load_reg : std_logic;
  signal fu_Streamout2_t1_load_reg : std_logic;
  signal fu_Streamin3_t1_load_reg : std_logic;
  signal fu_Streamout3_t1_load_reg : std_logic;
  signal fu_VALU_1_int0_load_reg : std_logic;
  signal fu_VALU_1_in2_load_reg : std_logic;
  signal fu_VALU_1_in1_load_reg : std_logic;
  signal fu_VALU_1_opc_reg : std_logic_vector(2 downto 0);
  signal fu_gcu_pc_load_reg : std_logic;
  signal fu_gcu_ra_load_reg : std_logic;
  signal fu_gcu_opc_reg : std_logic_vector(0 downto 0);

  -- RF control signals
  signal rf_BOOL_wr_load_reg : std_logic;
  signal rf_BOOL_wr_opc_reg : std_logic_vector(0 downto 0);
  signal rf_BOOL_rd_load_reg : std_logic;
  signal rf_BOOL_rd_opc_reg : std_logic_vector(0 downto 0);
  signal rf_RF_32_0_R1_32_load_reg : std_logic;
  signal rf_RF_32_0_R1_32_opc_reg : std_logic_vector(4 downto 0);
  signal rf_RF_32_0_W1_32_load_reg : std_logic;
  signal rf_RF_32_0_W1_32_opc_reg : std_logic_vector(4 downto 0);
  signal rf_RF_32_0_R2_load_reg : std_logic;
  signal rf_RF_32_0_R2_opc_reg : std_logic_vector(4 downto 0);
  signal rf_RF_256_0_R1_32_load_reg : std_logic;
  signal rf_RF_256_0_R1_32_opc_reg : std_logic_vector(4 downto 0);
  signal rf_RF_256_0_W1_32_load_reg : std_logic;
  signal rf_RF_256_0_W1_32_opc_reg : std_logic_vector(4 downto 0);
  signal rf_RF_256_0_R2_load_reg : std_logic;
  signal rf_RF_256_0_R2_opc_reg : std_logic_vector(4 downto 0);

  signal merged_glock_req : std_logic;
  signal pre_decode_merged_glock : std_logic;
  signal post_decode_merged_glock : std_logic;
  signal post_decode_merged_glock_r : std_logic;

  signal decode_fill_lock_reg : std_logic;
begin

  -- dismembering of instruction
  process (instructionword)
  begin --process
    move_B1 <= instructionword(23-1 downto 0);
    src_B1 <= instructionword(19 downto 7);
    dst_B1 <= instructionword(6 downto 0);
    grd_B1 <= instructionword(22 downto 20);
    move_B2 <= instructionword(41-1 downto 23);
    src_B2 <= instructionword(37 downto 29);
    dst_B2 <= instructionword(28 downto 23);
    grd_B2 <= instructionword(40 downto 38);
    move_B5_3 <= instructionword(59-1 downto 41);
    src_B5_3 <= instructionword(55 downto 47);
    dst_B5_3 <= instructionword(46 downto 41);
    grd_B5_3 <= instructionword(58 downto 56);
    move_B5_1_1_2_1 <= instructionword(74-1 downto 59);
    src_B5_1_1_2_1 <= instructionword(70 downto 65);
    dst_B5_1_1_2_1 <= instructionword(64 downto 59);
    grd_B5_1_1_2_1 <= instructionword(73 downto 71);
    move_B5_1_1_2_1_1 <= instructionword(81-1 downto 74);
    src_B5_1_1_2_1_1 <= instructionword(77 downto 77);
    dst_B5_1_1_2_1_1 <= instructionword(76 downto 74);
    grd_B5_1_1_2_1_1 <= instructionword(80 downto 78);
    move_B5_1_1_2_1_1_1_1_1 <= instructionword(91-1 downto 81);
    src_B5_1_1_2_1_1_1_1_1 <= instructionword(87 downto 86);
    dst_B5_1_1_2_1_1_1_1_1 <= instructionword(85 downto 81);
    grd_B5_1_1_2_1_1_1_1_1 <= instructionword(90 downto 88);
    move_B5_1_1_2_1_1_1_1_1_1_1 <= instructionword(98-1 downto 91);
    src_B5_1_1_2_1_1_1_1_1_1_1 <= instructionword(94 downto 93);
    dst_B5_1_1_2_1_1_1_1_1_1_1 <= instructionword(92 downto 91);
    grd_B5_1_1_2_1_1_1_1_1_1_1 <= instructionword(97 downto 95);
    move_B5_1_1_2_1_1_1_1_1_1_1_1 <= instructionword(112-1 downto 98);
    src_B5_1_1_2_1_1_1_1_1_1_1_1 <= instructionword(108 downto 103);
    dst_B5_1_1_2_1_1_1_1_1_1_1_1 <= instructionword(102 downto 98);
    grd_B5_1_1_2_1_1_1_1_1_1_1_1 <= instructionword(111 downto 109);
    move_B5_1_1_2_1_1_1_1_1_1_1_2 <= instructionword(123-1 downto 112);
    src_B5_1_1_2_1_1_1_1_1_1_1_2 <= instructionword(119 downto 114);
    dst_B5_1_1_2_1_1_1_1_1_1_1_2 <= instructionword(113 downto 112);
    grd_B5_1_1_2_1_1_1_1_1_1_1_2 <= instructionword(122 downto 120);
    move_B5_1_1_2_1_1_1_1_1_1_1_3 <= instructionword(138-1 downto 123);
    src_B5_1_1_2_1_1_1_1_1_1_1_3 <= instructionword(134 downto 129);
    dst_B5_1_1_2_1_1_1_1_1_1_1_3 <= instructionword(128 downto 123);
    grd_B5_1_1_2_1_1_1_1_1_1_1_3 <= instructionword(137 downto 135);
    move_StreamSock2 <= instructionword(171-1 downto 138);
    src_StreamSock2 <= instructionword(169 downto 138);

    limm_tag <= instructionword(171 downto 171);
  end process;

  -- map control registers to outputs
  fu_ALU_0_in1t_load <= fu_ALU_0_in1t_load_reg;
  fu_ALU_0_in2_load <= fu_ALU_0_in2_load_reg;
  fu_ALU_0_opc <= fu_ALU_0_opc_reg;

  fu_VALU_int0_load <= fu_VALU_int0_load_reg;
  fu_VALU_in2_load <= fu_VALU_in2_load_reg;
  fu_VALU_in1_load <= fu_VALU_in1_load_reg;
  fu_VALU_opc <= fu_VALU_opc_reg;

  fu_VBCAST_in1t_load <= fu_VBCAST_in1t_load_reg;

  fu_VSHUFFLE_in1t_load <= fu_VSHUFFLE_in1t_load_reg;
  fu_VSHUFFLE_in2_load <= fu_VSHUFFLE_in2_load_reg;
  fu_VSHUFFLE_opc <= fu_VSHUFFLE_opc_reg;

  fu_VEXTRACT_in1t_load <= fu_VEXTRACT_in1t_load_reg;
  fu_VEXTRACT_in2_load <= fu_VEXTRACT_in2_load_reg;

  fu_ALU_1_in1t_load <= fu_ALU_1_in1t_load_reg;
  fu_ALU_1_in2_load <= fu_ALU_1_in2_load_reg;
  fu_ALU_1_opc <= fu_ALU_1_opc_reg;

  fu_PARAM_LSU_in1t_load <= fu_PARAM_LSU_in1t_load_reg;
  fu_PARAM_LSU_in2_load <= fu_PARAM_LSU_in2_load_reg;
  fu_PARAM_LSU_opc <= fu_PARAM_LSU_opc_reg;

  fu_DATA_LSU_in1t_load <= fu_DATA_LSU_in1t_load_reg;
  fu_DATA_LSU_in2_load <= fu_DATA_LSU_in2_load_reg;
  fu_DATA_LSU_opc <= fu_DATA_LSU_opc_reg;

  fu_Streamin1_t1_load <= fu_Streamin1_t1_load_reg;

  fu_Streamin2_t1_load <= fu_Streamin2_t1_load_reg;

  fu_Streamout_t1_load <= fu_Streamout_t1_load_reg;

  fu_Streamout2_t1_load <= fu_Streamout2_t1_load_reg;

  fu_Streamin3_t1_load <= fu_Streamin3_t1_load_reg;

  fu_Streamout3_t1_load <= fu_Streamout3_t1_load_reg;

  fu_VALU_1_int0_load <= fu_VALU_1_int0_load_reg;
  fu_VALU_1_in2_load <= fu_VALU_1_in2_load_reg;
  fu_VALU_1_in1_load <= fu_VALU_1_in1_load_reg;
  fu_VALU_1_opc <= fu_VALU_1_opc_reg;

  ra_load <= fu_gcu_ra_load_reg;
  pc_load <= fu_gcu_pc_load_reg;
  pc_opcode <= fu_gcu_opc_reg;
  rf_BOOL_wr_load <= rf_BOOL_wr_load_reg;
  rf_BOOL_wr_opc <= rf_BOOL_wr_opc_reg;
  rf_BOOL_rd_load <= rf_BOOL_rd_load_reg;
  rf_BOOL_rd_opc <= rf_BOOL_rd_opc_reg;
  rf_RF_32_0_R1_32_load <= rf_RF_32_0_R1_32_load_reg;
  rf_RF_32_0_R1_32_opc <= rf_RF_32_0_R1_32_opc_reg;
  rf_RF_32_0_W1_32_load <= rf_RF_32_0_W1_32_load_reg;
  rf_RF_32_0_W1_32_opc <= rf_RF_32_0_W1_32_opc_reg;
  rf_RF_32_0_R2_load <= rf_RF_32_0_R2_load_reg;
  rf_RF_32_0_R2_opc <= rf_RF_32_0_R2_opc_reg;
  rf_RF_256_0_R1_32_load <= rf_RF_256_0_R1_32_load_reg;
  rf_RF_256_0_R1_32_opc <= rf_RF_256_0_R1_32_opc_reg;
  rf_RF_256_0_W1_32_load <= rf_RF_256_0_W1_32_load_reg;
  rf_RF_256_0_W1_32_opc <= rf_RF_256_0_W1_32_opc_reg;
  rf_RF_256_0_R2_load <= rf_RF_256_0_R2_load_reg;
  rf_RF_256_0_R2_opc <= rf_RF_256_0_R2_opc_reg;
  iu_IU_1x32_r0_read_opc <= "0";
  iu_IU_1x32_write_opc <= "0";
  socket_bool_i1_bus_cntrl <= socket_bool_i1_bus_cntrl_reg;
  socket_gcu_i1_bus_cntrl <= socket_gcu_i1_bus_cntrl_reg;
  socket_gcu_i2_bus_cntrl <= socket_gcu_i2_bus_cntrl_reg;
  socket_ALU_i1_bus_cntrl <= socket_ALU_i1_bus_cntrl_reg;
  socket_ALU_i2_bus_cntrl <= socket_ALU_i2_bus_cntrl_reg;
  socket_ALU_i1_1_1_1_1_bus_cntrl <= socket_ALU_i1_1_1_1_1_bus_cntrl_reg;
  socket_ALU_o1_1_1_2_1_bus_cntrl <= socket_ALU_o1_1_1_2_1_bus_cntrl_reg;
  socket_ALU_o1_1_1_2_1_1_bus_cntrl <= socket_ALU_o1_1_1_2_1_1_bus_cntrl_reg;
  socket_ALU_o1_1_1_2_1_2_bus_cntrl <= socket_ALU_o1_1_1_2_1_2_bus_cntrl_reg;
  socket_W1_32_1_bus_cntrl <= socket_W1_32_1_bus_cntrl_reg;
  socket_ALU_i1_1_1_1_1_1_1_bus_cntrl <= socket_ALU_i1_1_1_1_1_1_1_bus_cntrl_reg;
  socket_ALU_i1_1_1_1_2_1_bus_cntrl <= socket_ALU_i1_1_1_1_2_1_bus_cntrl_reg;
  socket_ALU_i2_1_bus_cntrl <= socket_ALU_i2_1_bus_cntrl_reg;
  socket_ALU_i2_2_bus_cntrl <= socket_ALU_i2_2_bus_cntrl_reg;
  socket_ALU_i1_1_1_1_2_bus_cntrl <= socket_ALU_i1_1_1_1_2_bus_cntrl_reg;
  socket_StreamSock1_bus_cntrl <= socket_StreamSock1_bus_cntrl_reg;
  socket_StreamSock2_bus_cntrl <= socket_StreamSock2_bus_cntrl_reg;
  socket_StreamSockV3_bus_cntrl <= socket_StreamSockV3_bus_cntrl_reg;
  socket_StreamSockV3_1_bus_cntrl <= socket_StreamSockV3_1_bus_cntrl_reg;
  socket_StreamSockV3_2_bus_cntrl <= socket_StreamSockV3_2_bus_cntrl_reg;
  socket_StreamSock2_1_bus_cntrl <= socket_StreamSock2_1_bus_cntrl_reg;
  socket_ALU_o1_1_1_2_1_2_1_bus_cntrl <= socket_ALU_o1_1_1_2_1_2_1_bus_cntrl_reg;
  socket_ALU_o1_1_1_2_1_1_1_bus_cntrl <= socket_ALU_o1_1_1_2_1_1_1_bus_cntrl_reg;
  socket_ALU_o1_1_1_2_1_3_bus_cntrl <= socket_ALU_o1_1_1_2_1_3_bus_cntrl_reg;
  B1_src_sel <= B1_src_sel_reg;
  B2_src_sel <= B2_src_sel_reg;
  B5_3_src_sel <= B5_3_src_sel_reg;
  B5_1_1_2_1_src_sel <= B5_1_1_2_1_src_sel_reg;
  B5_1_1_2_1_1_src_sel <= B5_1_1_2_1_1_src_sel_reg;
  B5_1_1_2_1_1_1_1_1_src_sel <= B5_1_1_2_1_1_1_1_1_src_sel_reg;
  B5_1_1_2_1_1_1_1_1_1_1_src_sel <= B5_1_1_2_1_1_1_1_1_1_1_src_sel_reg;
  B5_1_1_2_1_1_1_1_1_1_1_1_src_sel <= B5_1_1_2_1_1_1_1_1_1_1_1_src_sel_reg;
  B5_1_1_2_1_1_1_1_1_1_1_2_src_sel <= B5_1_1_2_1_1_1_1_1_1_1_2_src_sel_reg;
  B5_1_1_2_1_1_1_1_1_1_1_3_src_sel <= B5_1_1_2_1_1_1_1_1_1_1_3_src_sel_reg;
  simm_B1 <= simm_B1_reg;
  simm_B2 <= simm_B2_reg;
  simm_B5_3 <= simm_B5_3_reg;
  simm_StreamSock2 <= simm_StreamSock2_reg;

  -- generate signal squash_B1
  process (grd_B1, move_B1, rf_guard_BOOL_0, rf_guard_BOOL_1)
    variable sel : integer;
  begin --process
    -- squash by move NOP encoding
    if (conv_integer(unsigned(move_B1(22 downto 20))) = 5) then
      squash_B1 <= '1';
    else
      sel := conv_integer(unsigned(grd_B1));
      case sel is
        when 1 =>
          squash_B1 <= not rf_guard_BOOL_0;
        when 2 =>
          squash_B1 <= rf_guard_BOOL_0;
        when 3 =>
          squash_B1 <= not rf_guard_BOOL_1;
        when 4 =>
          squash_B1 <= rf_guard_BOOL_1;
        when others =>
          squash_B1 <= '0';
      end case;
    end if;
  end process;

  -- generate signal squash_B2
  process (grd_B2, limm_tag, move_B2, rf_guard_BOOL_0, rf_guard_BOOL_1)
    variable sel : integer;
  begin --process
    if (conv_integer(unsigned(limm_tag)) = 1) then
      squash_B2 <= '1';
    -- squash by move NOP encoding
    elsif (conv_integer(unsigned(move_B2(17 downto 15))) = 5) then
      squash_B2 <= '1';
    else
      sel := conv_integer(unsigned(grd_B2));
      case sel is
        when 1 =>
          squash_B2 <= not rf_guard_BOOL_0;
        when 2 =>
          squash_B2 <= rf_guard_BOOL_0;
        when 3 =>
          squash_B2 <= not rf_guard_BOOL_1;
        when 4 =>
          squash_B2 <= rf_guard_BOOL_1;
        when others =>
          squash_B2 <= '0';
      end case;
    end if;
  end process;

  -- generate signal squash_B5_3
  process (grd_B5_3, limm_tag, move_B5_3, rf_guard_BOOL_0, rf_guard_BOOL_1)
    variable sel : integer;
  begin --process
    if (conv_integer(unsigned(limm_tag)) = 1) then
      squash_B5_3 <= '1';
    -- squash by move NOP encoding
    elsif (conv_integer(unsigned(move_B5_3(17 downto 15))) = 5) then
      squash_B5_3 <= '1';
    else
      sel := conv_integer(unsigned(grd_B5_3));
      case sel is
        when 1 =>
          squash_B5_3 <= not rf_guard_BOOL_0;
        when 2 =>
          squash_B5_3 <= rf_guard_BOOL_0;
        when 3 =>
          squash_B5_3 <= not rf_guard_BOOL_1;
        when 4 =>
          squash_B5_3 <= rf_guard_BOOL_1;
        when others =>
          squash_B5_3 <= '0';
      end case;
    end if;
  end process;

  -- generate signal squash_B5_1_1_2_1
  process (grd_B5_1_1_2_1, move_B5_1_1_2_1, rf_guard_BOOL_0, rf_guard_BOOL_1)
    variable sel : integer;
  begin --process
    -- squash by move NOP encoding
    if (conv_integer(unsigned(move_B5_1_1_2_1(14 downto 12))) = 5) then
      squash_B5_1_1_2_1 <= '1';
    else
      sel := conv_integer(unsigned(grd_B5_1_1_2_1));
      case sel is
        when 1 =>
          squash_B5_1_1_2_1 <= not rf_guard_BOOL_0;
        when 2 =>
          squash_B5_1_1_2_1 <= rf_guard_BOOL_0;
        when 3 =>
          squash_B5_1_1_2_1 <= not rf_guard_BOOL_1;
        when 4 =>
          squash_B5_1_1_2_1 <= rf_guard_BOOL_1;
        when others =>
          squash_B5_1_1_2_1 <= '0';
      end case;
    end if;
  end process;

  -- generate signal squash_B5_1_1_2_1_1
  process (grd_B5_1_1_2_1_1, move_B5_1_1_2_1_1, rf_guard_BOOL_0, rf_guard_BOOL_1)
    variable sel : integer;
  begin --process
    -- squash by move NOP encoding
    if (conv_integer(unsigned(move_B5_1_1_2_1_1(6 downto 4))) = 5) then
      squash_B5_1_1_2_1_1 <= '1';
    else
      sel := conv_integer(unsigned(grd_B5_1_1_2_1_1));
      case sel is
        when 1 =>
          squash_B5_1_1_2_1_1 <= not rf_guard_BOOL_0;
        when 2 =>
          squash_B5_1_1_2_1_1 <= rf_guard_BOOL_0;
        when 3 =>
          squash_B5_1_1_2_1_1 <= not rf_guard_BOOL_1;
        when 4 =>
          squash_B5_1_1_2_1_1 <= rf_guard_BOOL_1;
        when others =>
          squash_B5_1_1_2_1_1 <= '0';
      end case;
    end if;
  end process;

  -- generate signal squash_B5_1_1_2_1_1_1_1_1
  process (grd_B5_1_1_2_1_1_1_1_1, move_B5_1_1_2_1_1_1_1_1, rf_guard_BOOL_0, rf_guard_BOOL_1)
    variable sel : integer;
  begin --process
    -- squash by move NOP encoding
    if (conv_integer(unsigned(move_B5_1_1_2_1_1_1_1_1(9 downto 7))) = 5) then
      squash_B5_1_1_2_1_1_1_1_1 <= '1';
    else
      sel := conv_integer(unsigned(grd_B5_1_1_2_1_1_1_1_1));
      case sel is
        when 1 =>
          squash_B5_1_1_2_1_1_1_1_1 <= not rf_guard_BOOL_0;
        when 2 =>
          squash_B5_1_1_2_1_1_1_1_1 <= rf_guard_BOOL_0;
        when 3 =>
          squash_B5_1_1_2_1_1_1_1_1 <= not rf_guard_BOOL_1;
        when 4 =>
          squash_B5_1_1_2_1_1_1_1_1 <= rf_guard_BOOL_1;
        when others =>
          squash_B5_1_1_2_1_1_1_1_1 <= '0';
      end case;
    end if;
  end process;

  -- generate signal squash_B5_1_1_2_1_1_1_1_1_1_1
  process (grd_B5_1_1_2_1_1_1_1_1_1_1, move_B5_1_1_2_1_1_1_1_1_1_1, rf_guard_BOOL_0, rf_guard_BOOL_1)
    variable sel : integer;
  begin --process
    -- squash by move NOP encoding
    if (conv_integer(unsigned(move_B5_1_1_2_1_1_1_1_1_1_1(6 downto 4))) = 5) then
      squash_B5_1_1_2_1_1_1_1_1_1_1 <= '1';
    else
      sel := conv_integer(unsigned(grd_B5_1_1_2_1_1_1_1_1_1_1));
      case sel is
        when 1 =>
          squash_B5_1_1_2_1_1_1_1_1_1_1 <= not rf_guard_BOOL_0;
        when 2 =>
          squash_B5_1_1_2_1_1_1_1_1_1_1 <= rf_guard_BOOL_0;
        when 3 =>
          squash_B5_1_1_2_1_1_1_1_1_1_1 <= not rf_guard_BOOL_1;
        when 4 =>
          squash_B5_1_1_2_1_1_1_1_1_1_1 <= rf_guard_BOOL_1;
        when others =>
          squash_B5_1_1_2_1_1_1_1_1_1_1 <= '0';
      end case;
    end if;
  end process;

  -- generate signal squash_B5_1_1_2_1_1_1_1_1_1_1_1
  process (grd_B5_1_1_2_1_1_1_1_1_1_1_1, move_B5_1_1_2_1_1_1_1_1_1_1_1, rf_guard_BOOL_0, rf_guard_BOOL_1)
    variable sel : integer;
  begin --process
    -- squash by move NOP encoding
    if (conv_integer(unsigned(move_B5_1_1_2_1_1_1_1_1_1_1_1(13 downto 11))) = 5) then
      squash_B5_1_1_2_1_1_1_1_1_1_1_1 <= '1';
    else
      sel := conv_integer(unsigned(grd_B5_1_1_2_1_1_1_1_1_1_1_1));
      case sel is
        when 1 =>
          squash_B5_1_1_2_1_1_1_1_1_1_1_1 <= not rf_guard_BOOL_0;
        when 2 =>
          squash_B5_1_1_2_1_1_1_1_1_1_1_1 <= rf_guard_BOOL_0;
        when 3 =>
          squash_B5_1_1_2_1_1_1_1_1_1_1_1 <= not rf_guard_BOOL_1;
        when 4 =>
          squash_B5_1_1_2_1_1_1_1_1_1_1_1 <= rf_guard_BOOL_1;
        when others =>
          squash_B5_1_1_2_1_1_1_1_1_1_1_1 <= '0';
      end case;
    end if;
  end process;

  -- generate signal squash_B5_1_1_2_1_1_1_1_1_1_1_2
  process (grd_B5_1_1_2_1_1_1_1_1_1_1_2, move_B5_1_1_2_1_1_1_1_1_1_1_2, rf_guard_BOOL_0, rf_guard_BOOL_1)
    variable sel : integer;
  begin --process
    -- squash by move NOP encoding
    if (conv_integer(unsigned(move_B5_1_1_2_1_1_1_1_1_1_1_2(10 downto 8))) = 5) then
      squash_B5_1_1_2_1_1_1_1_1_1_1_2 <= '1';
    else
      sel := conv_integer(unsigned(grd_B5_1_1_2_1_1_1_1_1_1_1_2));
      case sel is
        when 1 =>
          squash_B5_1_1_2_1_1_1_1_1_1_1_2 <= not rf_guard_BOOL_0;
        when 2 =>
          squash_B5_1_1_2_1_1_1_1_1_1_1_2 <= rf_guard_BOOL_0;
        when 3 =>
          squash_B5_1_1_2_1_1_1_1_1_1_1_2 <= not rf_guard_BOOL_1;
        when 4 =>
          squash_B5_1_1_2_1_1_1_1_1_1_1_2 <= rf_guard_BOOL_1;
        when others =>
          squash_B5_1_1_2_1_1_1_1_1_1_1_2 <= '0';
      end case;
    end if;
  end process;

  -- generate signal squash_B5_1_1_2_1_1_1_1_1_1_1_3
  process (grd_B5_1_1_2_1_1_1_1_1_1_1_3, move_B5_1_1_2_1_1_1_1_1_1_1_3, rf_guard_BOOL_0, rf_guard_BOOL_1)
    variable sel : integer;
  begin --process
    -- squash by move NOP encoding
    if (conv_integer(unsigned(move_B5_1_1_2_1_1_1_1_1_1_1_3(14 downto 12))) = 5) then
      squash_B5_1_1_2_1_1_1_1_1_1_1_3 <= '1';
    else
      sel := conv_integer(unsigned(grd_B5_1_1_2_1_1_1_1_1_1_1_3));
      case sel is
        when 1 =>
          squash_B5_1_1_2_1_1_1_1_1_1_1_3 <= not rf_guard_BOOL_0;
        when 2 =>
          squash_B5_1_1_2_1_1_1_1_1_1_1_3 <= rf_guard_BOOL_0;
        when 3 =>
          squash_B5_1_1_2_1_1_1_1_1_1_1_3 <= not rf_guard_BOOL_1;
        when 4 =>
          squash_B5_1_1_2_1_1_1_1_1_1_1_3 <= rf_guard_BOOL_1;
        when others =>
          squash_B5_1_1_2_1_1_1_1_1_1_1_3 <= '0';
      end case;
    end if;
  end process;

  -- generate signal squash_StreamSock2
  process (move_StreamSock2)
  begin --process
    -- squash by move NOP encoding
    if (move_StreamSock2(32) = '1') then
      squash_StreamSock2 <= '1';
    else
      squash_StreamSock2 <= '0';
    end if;
  end process;


  --long immediate write process
  process (clk, rstx)
  begin --process
    if (rstx = '0') then
      iu_IU_1x32_write_load <= '0';
      iu_IU_1x32_write <= (others => '0');
    elsif (clk'event and clk = '1') then
      if pre_decode_merged_glock = '0' then
        if (conv_integer(unsigned(limm_tag)) = 0) then
          iu_IU_1x32_write_load <= '0';
          iu_IU_1x32_write(31 downto 0) <= tce_sxt("0", 32);
        else
          iu_IU_1x32_write(31 downto 16) <= tce_ext(instructionword(56 downto 41), 16);
          iu_IU_1x32_write(15 downto 0) <= instructionword(38 downto 23);
          iu_IU_1x32_write_load <= '1';
        end if;
      end if;
    end if;
  end process;


  -- main decoding process
  process (clk, rstx)
  begin
    if (rstx = '0') then
      socket_bool_i1_bus_cntrl_reg <= (others => '0');
      socket_bool_o1_bus_cntrl_reg <= (others => '0');
      socket_gcu_i1_bus_cntrl_reg <= (others => '0');
      socket_gcu_i2_bus_cntrl_reg <= (others => '0');
      socket_gcu_o1_bus_cntrl_reg <= (others => '0');
      socket_ALU_i1_bus_cntrl_reg <= (others => '0');
      socket_ALU_i2_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_bus_cntrl_reg <= (others => '0');
      socket_IMM_rd_bus_cntrl_reg <= (others => '0');
      socket_R1_32_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_1_bus_cntrl_reg <= (others => '0');
      socket_ALU_i1_1_1_1_1_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_1_1_1_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_1_1_3_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_1_1_2_1_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_1_1_2_1_1_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_1_1_2_1_2_bus_cntrl_reg <= (others => '0');
      socket_W1_32_1_bus_cntrl_reg <= (others => '0');
      socket_R1_32_1_bus_cntrl_reg <= (others => '0');
      socket_ALU_i1_1_1_1_1_1_1_bus_cntrl_reg <= (others => '0');
      socket_ALU_i1_1_1_1_2_1_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_1_2_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_1_2_1_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_1_1_1_2_bus_cntrl_reg <= (others => '0');
      socket_ALU_i2_1_bus_cntrl_reg <= (others => '0');
      socket_ALU_i2_2_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_2_bus_cntrl_reg <= (others => '0');
      socket_R1_32_2_bus_cntrl_reg <= (others => '0');
      socket_R1_32_1_2_bus_cntrl_reg <= (others => '0');
      socket_ALU_i1_1_1_1_2_bus_cntrl_reg <= (others => '0');
      socket_StreamSock1_bus_cntrl_reg <= (others => '0');
      socket_StreamSock2_bus_cntrl_reg <= (others => '0');
      socket_StreamSockV1_bus_cntrl_reg <= (others => '0');
      socket_StreamSockV2_bus_cntrl_reg <= (others => '0');
      socket_StreamSockV3_bus_cntrl_reg <= (others => '0');
      socket_StreamSockV3_1_bus_cntrl_reg <= (others => '0');
      socket_StreamSockV3_2_bus_cntrl_reg <= (others => '0');
      socket_StreamSockV2_1_bus_cntrl_reg <= (others => '0');
      socket_StreamSock2_1_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_1_1_2_1_2_1_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_1_1_2_1_1_1_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_1_1_2_1_3_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_1_1_3_1_bus_cntrl_reg <= (others => '0');
      simm_B1_reg <= (others => '0');
      B1_src_sel_reg <= (others => '0');
      simm_B2_reg <= (others => '0');
      B2_src_sel_reg <= (others => '0');
      simm_B5_3_reg <= (others => '0');
      B5_3_src_sel_reg <= (others => '0');
      B5_1_1_2_1_src_sel_reg <= (others => '0');
      B5_1_1_2_1_1_src_sel_reg <= (others => '0');
      B5_1_1_2_1_1_1_1_1_src_sel_reg <= (others => '0');
      B5_1_1_2_1_1_1_1_1_1_1_src_sel_reg <= (others => '0');
      B5_1_1_2_1_1_1_1_1_1_1_1_src_sel_reg <= (others => '0');
      B5_1_1_2_1_1_1_1_1_1_1_2_src_sel_reg <= (others => '0');
      B5_1_1_2_1_1_1_1_1_1_1_3_src_sel_reg <= (others => '0');
      simm_StreamSock2_reg <= (others => '0');
      fu_ALU_0_opc_reg <= (others => '0');
      fu_VALU_opc_reg <= (others => '0');
      fu_VSHUFFLE_opc_reg <= (others => '0');
      fu_ALU_1_opc_reg <= (others => '0');
      fu_PARAM_LSU_opc_reg <= (others => '0');
      fu_DATA_LSU_opc_reg <= (others => '0');
      fu_VALU_1_opc_reg <= (others => '0');
      fu_gcu_opc_reg <= (others => '0');
      rf_BOOL_wr_opc_reg <= (others => '0');
      rf_BOOL_rd_opc_reg <= (others => '0');
      rf_RF_32_0_R1_32_opc_reg <= (others => '0');
      rf_RF_32_0_W1_32_opc_reg <= (others => '0');
      rf_RF_32_0_R2_opc_reg <= (others => '0');
      rf_RF_256_0_R1_32_opc_reg <= (others => '0');
      rf_RF_256_0_W1_32_opc_reg <= (others => '0');
      rf_RF_256_0_R2_opc_reg <= (others => '0');

      fu_ALU_0_in1t_load_reg <= '0';
      fu_ALU_0_in2_load_reg <= '0';
      fu_VALU_int0_load_reg <= '0';
      fu_VALU_in2_load_reg <= '0';
      fu_VALU_in1_load_reg <= '0';
      fu_VBCAST_in1t_load_reg <= '0';
      fu_VSHUFFLE_in1t_load_reg <= '0';
      fu_VSHUFFLE_in2_load_reg <= '0';
      fu_VEXTRACT_in1t_load_reg <= '0';
      fu_VEXTRACT_in2_load_reg <= '0';
      fu_ALU_1_in1t_load_reg <= '0';
      fu_ALU_1_in2_load_reg <= '0';
      fu_PARAM_LSU_in1t_load_reg <= '0';
      fu_PARAM_LSU_in2_load_reg <= '0';
      fu_DATA_LSU_in1t_load_reg <= '0';
      fu_DATA_LSU_in2_load_reg <= '0';
      fu_Streamin1_t1_load_reg <= '0';
      fu_Streamin2_t1_load_reg <= '0';
      fu_Streamout_t1_load_reg <= '0';
      fu_Streamout2_t1_load_reg <= '0';
      fu_Streamin3_t1_load_reg <= '0';
      fu_Streamout3_t1_load_reg <= '0';
      fu_VALU_1_int0_load_reg <= '0';
      fu_VALU_1_in2_load_reg <= '0';
      fu_VALU_1_in1_load_reg <= '0';
      fu_gcu_pc_load_reg <= '0';
      fu_gcu_ra_load_reg <= '0';
      rf_BOOL_wr_load_reg <= '0';
      rf_BOOL_rd_load_reg <= '0';
      rf_RF_32_0_R1_32_load_reg <= '0';
      rf_RF_32_0_W1_32_load_reg <= '0';
      rf_RF_32_0_R2_load_reg <= '0';
      rf_RF_256_0_R1_32_load_reg <= '0';
      rf_RF_256_0_W1_32_load_reg <= '0';
      rf_RF_256_0_R2_load_reg <= '0';
      iu_IU_1x32_r0_read_load <= '0';


    elsif (clk'event and clk = '1') then -- rising clock edge
      if (db_tta_nreset = '0') then
      socket_bool_i1_bus_cntrl_reg <= (others => '0');
      socket_bool_o1_bus_cntrl_reg <= (others => '0');
      socket_gcu_i1_bus_cntrl_reg <= (others => '0');
      socket_gcu_i2_bus_cntrl_reg <= (others => '0');
      socket_gcu_o1_bus_cntrl_reg <= (others => '0');
      socket_ALU_i1_bus_cntrl_reg <= (others => '0');
      socket_ALU_i2_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_bus_cntrl_reg <= (others => '0');
      socket_IMM_rd_bus_cntrl_reg <= (others => '0');
      socket_R1_32_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_1_bus_cntrl_reg <= (others => '0');
      socket_ALU_i1_1_1_1_1_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_1_1_1_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_1_1_3_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_1_1_2_1_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_1_1_2_1_1_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_1_1_2_1_2_bus_cntrl_reg <= (others => '0');
      socket_W1_32_1_bus_cntrl_reg <= (others => '0');
      socket_R1_32_1_bus_cntrl_reg <= (others => '0');
      socket_ALU_i1_1_1_1_1_1_1_bus_cntrl_reg <= (others => '0');
      socket_ALU_i1_1_1_1_2_1_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_1_2_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_1_2_1_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_1_1_1_2_bus_cntrl_reg <= (others => '0');
      socket_ALU_i2_1_bus_cntrl_reg <= (others => '0');
      socket_ALU_i2_2_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_2_bus_cntrl_reg <= (others => '0');
      socket_R1_32_2_bus_cntrl_reg <= (others => '0');
      socket_R1_32_1_2_bus_cntrl_reg <= (others => '0');
      socket_ALU_i1_1_1_1_2_bus_cntrl_reg <= (others => '0');
      socket_StreamSock1_bus_cntrl_reg <= (others => '0');
      socket_StreamSock2_bus_cntrl_reg <= (others => '0');
      socket_StreamSockV1_bus_cntrl_reg <= (others => '0');
      socket_StreamSockV2_bus_cntrl_reg <= (others => '0');
      socket_StreamSockV3_bus_cntrl_reg <= (others => '0');
      socket_StreamSockV3_1_bus_cntrl_reg <= (others => '0');
      socket_StreamSockV3_2_bus_cntrl_reg <= (others => '0');
      socket_StreamSockV2_1_bus_cntrl_reg <= (others => '0');
      socket_StreamSock2_1_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_1_1_2_1_2_1_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_1_1_2_1_1_1_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_1_1_2_1_3_bus_cntrl_reg <= (others => '0');
      socket_ALU_o1_1_1_3_1_bus_cntrl_reg <= (others => '0');
      simm_B1_reg <= (others => '0');
      B1_src_sel_reg <= (others => '0');
      simm_B2_reg <= (others => '0');
      B2_src_sel_reg <= (others => '0');
      simm_B5_3_reg <= (others => '0');
      B5_3_src_sel_reg <= (others => '0');
      B5_1_1_2_1_src_sel_reg <= (others => '0');
      B5_1_1_2_1_1_src_sel_reg <= (others => '0');
      B5_1_1_2_1_1_1_1_1_src_sel_reg <= (others => '0');
      B5_1_1_2_1_1_1_1_1_1_1_src_sel_reg <= (others => '0');
      B5_1_1_2_1_1_1_1_1_1_1_1_src_sel_reg <= (others => '0');
      B5_1_1_2_1_1_1_1_1_1_1_2_src_sel_reg <= (others => '0');
      B5_1_1_2_1_1_1_1_1_1_1_3_src_sel_reg <= (others => '0');
      simm_StreamSock2_reg <= (others => '0');
      fu_ALU_0_opc_reg <= (others => '0');
      fu_VALU_opc_reg <= (others => '0');
      fu_VSHUFFLE_opc_reg <= (others => '0');
      fu_ALU_1_opc_reg <= (others => '0');
      fu_PARAM_LSU_opc_reg <= (others => '0');
      fu_DATA_LSU_opc_reg <= (others => '0');
      fu_VALU_1_opc_reg <= (others => '0');
      fu_gcu_opc_reg <= (others => '0');
      rf_BOOL_wr_opc_reg <= (others => '0');
      rf_BOOL_rd_opc_reg <= (others => '0');
      rf_RF_32_0_R1_32_opc_reg <= (others => '0');
      rf_RF_32_0_W1_32_opc_reg <= (others => '0');
      rf_RF_32_0_R2_opc_reg <= (others => '0');
      rf_RF_256_0_R1_32_opc_reg <= (others => '0');
      rf_RF_256_0_W1_32_opc_reg <= (others => '0');
      rf_RF_256_0_R2_opc_reg <= (others => '0');

      fu_ALU_0_in1t_load_reg <= '0';
      fu_ALU_0_in2_load_reg <= '0';
      fu_VALU_int0_load_reg <= '0';
      fu_VALU_in2_load_reg <= '0';
      fu_VALU_in1_load_reg <= '0';
      fu_VBCAST_in1t_load_reg <= '0';
      fu_VSHUFFLE_in1t_load_reg <= '0';
      fu_VSHUFFLE_in2_load_reg <= '0';
      fu_VEXTRACT_in1t_load_reg <= '0';
      fu_VEXTRACT_in2_load_reg <= '0';
      fu_ALU_1_in1t_load_reg <= '0';
      fu_ALU_1_in2_load_reg <= '0';
      fu_PARAM_LSU_in1t_load_reg <= '0';
      fu_PARAM_LSU_in2_load_reg <= '0';
      fu_DATA_LSU_in1t_load_reg <= '0';
      fu_DATA_LSU_in2_load_reg <= '0';
      fu_Streamin1_t1_load_reg <= '0';
      fu_Streamin2_t1_load_reg <= '0';
      fu_Streamout_t1_load_reg <= '0';
      fu_Streamout2_t1_load_reg <= '0';
      fu_Streamin3_t1_load_reg <= '0';
      fu_Streamout3_t1_load_reg <= '0';
      fu_VALU_1_int0_load_reg <= '0';
      fu_VALU_1_in2_load_reg <= '0';
      fu_VALU_1_in1_load_reg <= '0';
      fu_gcu_pc_load_reg <= '0';
      fu_gcu_ra_load_reg <= '0';
      rf_BOOL_wr_load_reg <= '0';
      rf_BOOL_rd_load_reg <= '0';
      rf_RF_32_0_R1_32_load_reg <= '0';
      rf_RF_32_0_W1_32_load_reg <= '0';
      rf_RF_32_0_R2_load_reg <= '0';
      rf_RF_256_0_R1_32_load_reg <= '0';
      rf_RF_256_0_W1_32_load_reg <= '0';
      rf_RF_256_0_R2_load_reg <= '0';
      iu_IU_1x32_r0_read_load <= '0';

      elsif (pre_decode_merged_glock = '0') then

        -- bus control signals for output mux
        if (squash_B1 = '0' and conv_integer(unsigned(src_B1(12 downto 8))) = 17) then
          B1_src_sel_reg <= std_logic_vector(conv_unsigned(0, B1_src_sel_reg'length));
        elsif (squash_B1 = '0' and conv_integer(unsigned(src_B1(12 downto 8))) = 18) then
          B1_src_sel_reg <= std_logic_vector(conv_unsigned(1, B1_src_sel_reg'length));
        elsif (squash_B1 = '0' and conv_integer(unsigned(src_B1(12 downto 8))) = 19) then
          B1_src_sel_reg <= std_logic_vector(conv_unsigned(2, B1_src_sel_reg'length));
        elsif (squash_B1 = '0' and conv_integer(unsigned(src_B1(12 downto 8))) = 20) then
          B1_src_sel_reg <= std_logic_vector(conv_unsigned(3, B1_src_sel_reg'length));
        elsif (squash_B1 = '0' and conv_integer(unsigned(src_B1(12 downto 8))) = 16) then
          B1_src_sel_reg <= std_logic_vector(conv_unsigned(4, B1_src_sel_reg'length));
        elsif (squash_B1 = '0' and conv_integer(unsigned(src_B1(12 downto 8))) = 21) then
          B1_src_sel_reg <= std_logic_vector(conv_unsigned(5, B1_src_sel_reg'length));
        elsif (squash_B1 = '0' and conv_integer(unsigned(src_B1(12 downto 8))) = 22) then
          B1_src_sel_reg <= std_logic_vector(conv_unsigned(6, B1_src_sel_reg'length));
        elsif (squash_B1 = '0' and conv_integer(unsigned(src_B1(12 downto 8))) = 23) then
          B1_src_sel_reg <= std_logic_vector(conv_unsigned(7, B1_src_sel_reg'length));
        elsif (squash_B1 = '0' and conv_integer(unsigned(src_B1(12 downto 8))) = 24) then
          B1_src_sel_reg <= std_logic_vector(conv_unsigned(8, B1_src_sel_reg'length));
        elsif (squash_B1 = '0' and conv_integer(unsigned(src_B1(12 downto 12))) = 0) then
          B1_src_sel_reg <= std_logic_vector(conv_unsigned(9, B1_src_sel_reg'length));
        end if;
        if (squash_B1 = '0' and conv_integer(unsigned(src_B1(12 downto 12))) = 0) then
        simm_B1_reg <= tce_sxt(src_B1(11 downto 0), simm_B1_reg'length);
        end if;
        if (squash_B2 = '0' and conv_integer(unsigned(src_B2(8 downto 5))) = 10) then
          B2_src_sel_reg <= std_logic_vector(conv_unsigned(0, B2_src_sel_reg'length));
        elsif (squash_B2 = '0' and conv_integer(unsigned(src_B2(8 downto 5))) = 11) then
          B2_src_sel_reg <= std_logic_vector(conv_unsigned(1, B2_src_sel_reg'length));
        elsif (squash_B2 = '0' and conv_integer(unsigned(src_B2(8 downto 5))) = 12) then
          B2_src_sel_reg <= std_logic_vector(conv_unsigned(2, B2_src_sel_reg'length));
        elsif (squash_B2 = '0' and conv_integer(unsigned(src_B2(8 downto 5))) = 13) then
          B2_src_sel_reg <= std_logic_vector(conv_unsigned(3, B2_src_sel_reg'length));
        elsif (squash_B2 = '0' and conv_integer(unsigned(src_B2(8 downto 5))) = 8) then
          B2_src_sel_reg <= std_logic_vector(conv_unsigned(4, B2_src_sel_reg'length));
        elsif (squash_B2 = '0' and conv_integer(unsigned(src_B2(8 downto 5))) = 14) then
          B2_src_sel_reg <= std_logic_vector(conv_unsigned(5, B2_src_sel_reg'length));
        elsif (squash_B2 = '0' and conv_integer(unsigned(src_B2(8 downto 5))) = 15) then
          B2_src_sel_reg <= std_logic_vector(conv_unsigned(6, B2_src_sel_reg'length));
        elsif (squash_B2 = '0' and conv_integer(unsigned(src_B2(8 downto 5))) = 9) then
          B2_src_sel_reg <= std_logic_vector(conv_unsigned(7, B2_src_sel_reg'length));
        elsif (squash_B2 = '0' and conv_integer(unsigned(src_B2(8 downto 8))) = 0) then
          B2_src_sel_reg <= std_logic_vector(conv_unsigned(8, B2_src_sel_reg'length));
        end if;
        if (squash_B2 = '0' and conv_integer(unsigned(src_B2(8 downto 8))) = 0) then
        simm_B2_reg <= tce_sxt(src_B2(7 downto 0), simm_B2_reg'length);
        end if;
        if (squash_B5_3 = '0' and conv_integer(unsigned(src_B5_3(8 downto 5))) = 9) then
          B5_3_src_sel_reg <= std_logic_vector(conv_unsigned(0, B5_3_src_sel_reg'length));
        elsif (squash_B5_3 = '0' and conv_integer(unsigned(src_B5_3(8 downto 5))) = 10) then
          B5_3_src_sel_reg <= std_logic_vector(conv_unsigned(1, B5_3_src_sel_reg'length));
        elsif (squash_B5_3 = '0' and conv_integer(unsigned(src_B5_3(8 downto 5))) = 11) then
          B5_3_src_sel_reg <= std_logic_vector(conv_unsigned(2, B5_3_src_sel_reg'length));
        elsif (squash_B5_3 = '0' and conv_integer(unsigned(src_B5_3(8 downto 5))) = 12) then
          B5_3_src_sel_reg <= std_logic_vector(conv_unsigned(3, B5_3_src_sel_reg'length));
        elsif (squash_B5_3 = '0' and conv_integer(unsigned(src_B5_3(8 downto 5))) = 13) then
          B5_3_src_sel_reg <= std_logic_vector(conv_unsigned(4, B5_3_src_sel_reg'length));
        elsif (squash_B5_3 = '0' and conv_integer(unsigned(src_B5_3(8 downto 5))) = 8) then
          B5_3_src_sel_reg <= std_logic_vector(conv_unsigned(5, B5_3_src_sel_reg'length));
        elsif (squash_B5_3 = '0' and conv_integer(unsigned(src_B5_3(8 downto 8))) = 0) then
          B5_3_src_sel_reg <= std_logic_vector(conv_unsigned(6, B5_3_src_sel_reg'length));
        end if;
        if (squash_B5_3 = '0' and conv_integer(unsigned(src_B5_3(8 downto 8))) = 0) then
        simm_B5_3_reg <= tce_sxt(src_B5_3(7 downto 0), simm_B5_3_reg'length);
        end if;
        if (squash_B5_1_1_2_1 = '0' and conv_integer(unsigned(src_B5_1_1_2_1(5 downto 2))) = 8) then
          B5_1_1_2_1_src_sel_reg <= std_logic_vector(conv_unsigned(0, B5_1_1_2_1_src_sel_reg'length));
        elsif (squash_B5_1_1_2_1 = '0' and conv_integer(unsigned(src_B5_1_1_2_1(5 downto 2))) = 9) then
          B5_1_1_2_1_src_sel_reg <= std_logic_vector(conv_unsigned(1, B5_1_1_2_1_src_sel_reg'length));
        elsif (squash_B5_1_1_2_1 = '0' and conv_integer(unsigned(src_B5_1_1_2_1(5 downto 5))) = 0) then
          B5_1_1_2_1_src_sel_reg <= std_logic_vector(conv_unsigned(2, B5_1_1_2_1_src_sel_reg'length));
        elsif (squash_B5_1_1_2_1 = '0' and conv_integer(unsigned(src_B5_1_1_2_1(5 downto 2))) = 10) then
          B5_1_1_2_1_src_sel_reg <= std_logic_vector(conv_unsigned(3, B5_1_1_2_1_src_sel_reg'length));
        elsif (squash_B5_1_1_2_1 = '0' and conv_integer(unsigned(src_B5_1_1_2_1(5 downto 2))) = 11) then
          B5_1_1_2_1_src_sel_reg <= std_logic_vector(conv_unsigned(4, B5_1_1_2_1_src_sel_reg'length));
        elsif (squash_B5_1_1_2_1 = '0' and conv_integer(unsigned(src_B5_1_1_2_1(5 downto 2))) = 12) then
          B5_1_1_2_1_src_sel_reg <= std_logic_vector(conv_unsigned(5, B5_1_1_2_1_src_sel_reg'length));
        end if;
        if (squash_B5_1_1_2_1_1 = '0' and conv_integer(unsigned(src_B5_1_1_2_1_1(0 downto 0))) = 0) then
          B5_1_1_2_1_1_src_sel_reg <= std_logic_vector(conv_unsigned(0, B5_1_1_2_1_1_src_sel_reg'length));
        elsif (squash_B5_1_1_2_1_1 = '0' and conv_integer(unsigned(src_B5_1_1_2_1_1(0 downto 0))) = 1) then
          B5_1_1_2_1_1_src_sel_reg <= std_logic_vector(conv_unsigned(1, B5_1_1_2_1_1_src_sel_reg'length));
        end if;
        if (squash_B5_1_1_2_1_1_1_1_1 = '0' and conv_integer(unsigned(src_B5_1_1_2_1_1_1_1_1(1 downto 0))) = 0) then
          B5_1_1_2_1_1_1_1_1_src_sel_reg <= std_logic_vector(conv_unsigned(0, B5_1_1_2_1_1_1_1_1_src_sel_reg'length));
        elsif (squash_B5_1_1_2_1_1_1_1_1 = '0' and conv_integer(unsigned(src_B5_1_1_2_1_1_1_1_1(1 downto 0))) = 1) then
          B5_1_1_2_1_1_1_1_1_src_sel_reg <= std_logic_vector(conv_unsigned(1, B5_1_1_2_1_1_1_1_1_src_sel_reg'length));
        elsif (squash_B5_1_1_2_1_1_1_1_1 = '0' and conv_integer(unsigned(src_B5_1_1_2_1_1_1_1_1(1 downto 0))) = 2) then
          B5_1_1_2_1_1_1_1_1_src_sel_reg <= std_logic_vector(conv_unsigned(2, B5_1_1_2_1_1_1_1_1_src_sel_reg'length));
        end if;
        if (squash_B5_1_1_2_1_1_1_1_1_1_1 = '0' and conv_integer(unsigned(src_B5_1_1_2_1_1_1_1_1_1_1(1 downto 0))) = 0) then
          B5_1_1_2_1_1_1_1_1_1_1_src_sel_reg <= std_logic_vector(conv_unsigned(0, B5_1_1_2_1_1_1_1_1_1_1_src_sel_reg'length));
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1 = '0' and conv_integer(unsigned(src_B5_1_1_2_1_1_1_1_1_1_1(1 downto 0))) = 1) then
          B5_1_1_2_1_1_1_1_1_1_1_src_sel_reg <= std_logic_vector(conv_unsigned(1, B5_1_1_2_1_1_1_1_1_1_1_src_sel_reg'length));
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1 = '0' and conv_integer(unsigned(src_B5_1_1_2_1_1_1_1_1_1_1(1 downto 0))) = 2) then
          B5_1_1_2_1_1_1_1_1_1_1_src_sel_reg <= std_logic_vector(conv_unsigned(2, B5_1_1_2_1_1_1_1_1_1_1_src_sel_reg'length));
        end if;
        if (squash_B5_1_1_2_1_1_1_1_1_1_1_1 = '0' and conv_integer(unsigned(src_B5_1_1_2_1_1_1_1_1_1_1_1(5 downto 5))) = 0) then
          B5_1_1_2_1_1_1_1_1_1_1_1_src_sel_reg <= std_logic_vector(conv_unsigned(0, B5_1_1_2_1_1_1_1_1_1_1_1_src_sel_reg'length));
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1_1 = '0' and conv_integer(unsigned(src_B5_1_1_2_1_1_1_1_1_1_1_1(5 downto 4))) = 2) then
          B5_1_1_2_1_1_1_1_1_1_1_1_src_sel_reg <= std_logic_vector(conv_unsigned(1, B5_1_1_2_1_1_1_1_1_1_1_1_src_sel_reg'length));
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1_1 = '0' and conv_integer(unsigned(src_B5_1_1_2_1_1_1_1_1_1_1_1(5 downto 4))) = 3) then
          B5_1_1_2_1_1_1_1_1_1_1_1_src_sel_reg <= std_logic_vector(conv_unsigned(2, B5_1_1_2_1_1_1_1_1_1_1_1_src_sel_reg'length));
        end if;
        if (squash_B5_1_1_2_1_1_1_1_1_1_1_2 = '0' and conv_integer(unsigned(src_B5_1_1_2_1_1_1_1_1_1_1_2(5 downto 5))) = 0) then
          B5_1_1_2_1_1_1_1_1_1_1_2_src_sel_reg <= std_logic_vector(conv_unsigned(0, B5_1_1_2_1_1_1_1_1_1_1_2_src_sel_reg'length));
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1_2 = '0' and conv_integer(unsigned(src_B5_1_1_2_1_1_1_1_1_1_1_2(5 downto 5))) = 1) then
          B5_1_1_2_1_1_1_1_1_1_1_2_src_sel_reg <= std_logic_vector(conv_unsigned(1, B5_1_1_2_1_1_1_1_1_1_1_2_src_sel_reg'length));
        end if;
        if (squash_B5_1_1_2_1_1_1_1_1_1_1_3 = '0' and conv_integer(unsigned(src_B5_1_1_2_1_1_1_1_1_1_1_3(5 downto 2))) = 8) then
          B5_1_1_2_1_1_1_1_1_1_1_3_src_sel_reg <= std_logic_vector(conv_unsigned(0, B5_1_1_2_1_1_1_1_1_1_1_3_src_sel_reg'length));
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1_3 = '0' and conv_integer(unsigned(src_B5_1_1_2_1_1_1_1_1_1_1_3(5 downto 2))) = 9) then
          B5_1_1_2_1_1_1_1_1_1_1_3_src_sel_reg <= std_logic_vector(conv_unsigned(1, B5_1_1_2_1_1_1_1_1_1_1_3_src_sel_reg'length));
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1_3 = '0' and conv_integer(unsigned(src_B5_1_1_2_1_1_1_1_1_1_1_3(5 downto 5))) = 0) then
          B5_1_1_2_1_1_1_1_1_1_1_3_src_sel_reg <= std_logic_vector(conv_unsigned(2, B5_1_1_2_1_1_1_1_1_1_1_3_src_sel_reg'length));
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1_3 = '0' and conv_integer(unsigned(src_B5_1_1_2_1_1_1_1_1_1_1_3(5 downto 2))) = 10) then
          B5_1_1_2_1_1_1_1_1_1_1_3_src_sel_reg <= std_logic_vector(conv_unsigned(3, B5_1_1_2_1_1_1_1_1_1_1_3_src_sel_reg'length));
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1_3 = '0' and conv_integer(unsigned(src_B5_1_1_2_1_1_1_1_1_1_1_3(5 downto 2))) = 11) then
          B5_1_1_2_1_1_1_1_1_1_1_3_src_sel_reg <= std_logic_vector(conv_unsigned(4, B5_1_1_2_1_1_1_1_1_1_1_3_src_sel_reg'length));
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1_3 = '0' and conv_integer(unsigned(src_B5_1_1_2_1_1_1_1_1_1_1_3(5 downto 2))) = 12) then
          B5_1_1_2_1_1_1_1_1_1_1_3_src_sel_reg <= std_logic_vector(conv_unsigned(5, B5_1_1_2_1_1_1_1_1_1_1_3_src_sel_reg'length));
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1_3 = '0' and conv_integer(unsigned(src_B5_1_1_2_1_1_1_1_1_1_1_3(5 downto 2))) = 13) then
          B5_1_1_2_1_1_1_1_1_1_1_3_src_sel_reg <= std_logic_vector(conv_unsigned(6, B5_1_1_2_1_1_1_1_1_1_1_3_src_sel_reg'length));
        end if;
        if (squash_StreamSock2 = '0') then
        simm_StreamSock2_reg <= tce_ext(src_StreamSock2(31 downto 0), simm_StreamSock2_reg'length);
        end if;
        -- data control signals for output sockets connected to FUs
        -- control signals for RF read ports
        if (squash_B1 = '0' and conv_integer(unsigned(src_B1(12 downto 8))) = 17 and true) then
          rf_BOOL_rd_load_reg <= '1';
          rf_BOOL_rd_opc_reg <= tce_ext(src_B1(0 downto 0), rf_BOOL_rd_opc_reg'length);
        elsif (squash_B2 = '0' and conv_integer(unsigned(src_B2(8 downto 5))) = 10 and true) then
          rf_BOOL_rd_load_reg <= '1';
          rf_BOOL_rd_opc_reg <= tce_ext(src_B2(0 downto 0), rf_BOOL_rd_opc_reg'length);
        else
          rf_BOOL_rd_load_reg <= '0';
        end if;
        if (squash_B1 = '0' and conv_integer(unsigned(src_B1(12 downto 8))) = 16 and true) then
          rf_RF_32_0_R1_32_load_reg <= '1';
          rf_RF_32_0_R1_32_opc_reg <= tce_ext(src_B1(4 downto 0), rf_RF_32_0_R1_32_opc_reg'length);
        elsif (squash_B2 = '0' and conv_integer(unsigned(src_B2(8 downto 5))) = 8 and true) then
          rf_RF_32_0_R1_32_load_reg <= '1';
          rf_RF_32_0_R1_32_opc_reg <= tce_ext(src_B2(4 downto 0), rf_RF_32_0_R1_32_opc_reg'length);
        else
          rf_RF_32_0_R1_32_load_reg <= '0';
        end if;
        if (squash_B2 = '0' and conv_integer(unsigned(src_B2(8 downto 5))) = 9 and true) then
          rf_RF_32_0_R2_load_reg <= '1';
          rf_RF_32_0_R2_opc_reg <= tce_ext(src_B2(4 downto 0), rf_RF_32_0_R2_opc_reg'length);
        elsif (squash_B5_3 = '0' and conv_integer(unsigned(src_B5_3(8 downto 5))) = 8 and true) then
          rf_RF_32_0_R2_load_reg <= '1';
          rf_RF_32_0_R2_opc_reg <= tce_ext(src_B5_3(4 downto 0), rf_RF_32_0_R2_opc_reg'length);
        else
          rf_RF_32_0_R2_load_reg <= '0';
        end if;
        if (squash_B5_1_1_2_1_1_1_1_1_1_1_1 = '0' and conv_integer(unsigned(src_B5_1_1_2_1_1_1_1_1_1_1_1(5 downto 5))) = 0 and true) then
          rf_RF_256_0_W1_32_load_reg <= '1';
          rf_RF_256_0_W1_32_opc_reg <= tce_ext(src_B5_1_1_2_1_1_1_1_1_1_1_1(4 downto 0), rf_RF_256_0_W1_32_opc_reg'length);
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1_3 = '0' and conv_integer(unsigned(src_B5_1_1_2_1_1_1_1_1_1_1_3(5 downto 5))) = 0 and true) then
          rf_RF_256_0_W1_32_load_reg <= '1';
          rf_RF_256_0_W1_32_opc_reg <= tce_ext(src_B5_1_1_2_1_1_1_1_1_1_1_3(4 downto 0), rf_RF_256_0_W1_32_opc_reg'length);
        else
          rf_RF_256_0_W1_32_load_reg <= '0';
        end if;
        if (squash_B5_1_1_2_1 = '0' and conv_integer(unsigned(src_B5_1_1_2_1(5 downto 5))) = 0 and true) then
          rf_RF_256_0_R2_load_reg <= '1';
          rf_RF_256_0_R2_opc_reg <= tce_ext(src_B5_1_1_2_1(4 downto 0), rf_RF_256_0_R2_opc_reg'length);
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1_2 = '0' and conv_integer(unsigned(src_B5_1_1_2_1_1_1_1_1_1_1_2(5 downto 5))) = 0 and true) then
          rf_RF_256_0_R2_load_reg <= '1';
          rf_RF_256_0_R2_opc_reg <= tce_ext(src_B5_1_1_2_1_1_1_1_1_1_1_2(4 downto 0), rf_RF_256_0_R2_opc_reg'length);
        else
          rf_RF_256_0_R2_load_reg <= '0';
        end if;

        --control signals for IU read ports
        -- control signals for IU read ports
        if (squash_B1 = '0' and conv_integer(unsigned(src_B1(12 downto 8))) = 20) then
          iu_IU_1x32_r0_read_load <= '1';
        elsif (squash_B2 = '0' and conv_integer(unsigned(src_B2(8 downto 5))) = 13) then
          iu_IU_1x32_r0_read_load <= '1';
        elsif (squash_B5_3 = '0' and conv_integer(unsigned(src_B5_3(8 downto 5))) = 10) then
          iu_IU_1x32_r0_read_load <= '1';
        else
          iu_IU_1x32_r0_read_load <= '0';
        end if;

        -- control signals for FU inputs
        if (squash_B1 = '0' and conv_integer(unsigned(dst_B1(6 downto 4))) = 2) then
          fu_ALU_0_in1t_load_reg <= '1';
          fu_ALU_0_opc_reg <= dst_B1(3 downto 0);
          socket_ALU_i1_bus_cntrl_reg <= conv_std_logic_vector(1, socket_ALU_i1_bus_cntrl_reg'length);
        elsif (squash_B5_3 = '0' and conv_integer(unsigned(dst_B5_3(5 downto 4))) = 0) then
          fu_ALU_0_in1t_load_reg <= '1';
          fu_ALU_0_opc_reg <= dst_B5_3(3 downto 0);
          socket_ALU_i1_bus_cntrl_reg <= conv_std_logic_vector(0, socket_ALU_i1_bus_cntrl_reg'length);
        else
          fu_ALU_0_in1t_load_reg <= '0';
        end if;
        if (squash_B1 = '0' and conv_integer(unsigned(dst_B1(6 downto 0))) = 61) then
          fu_ALU_0_in2_load_reg <= '1';
          socket_ALU_i2_bus_cntrl_reg <= conv_std_logic_vector(1, socket_ALU_i2_bus_cntrl_reg'length);
        elsif (squash_B2 = '0' and conv_integer(unsigned(dst_B2(5 downto 0))) = 29) then
          fu_ALU_0_in2_load_reg <= '1';
          socket_ALU_i2_bus_cntrl_reg <= conv_std_logic_vector(0, socket_ALU_i2_bus_cntrl_reg'length);
        else
          fu_ALU_0_in2_load_reg <= '0';
        end if;
        if (squash_B5_1_1_2_1 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1(5 downto 3))) = 4) then
          fu_VALU_int0_load_reg <= '1';
          fu_VALU_opc_reg <= dst_B5_1_1_2_1(2 downto 0);
          socket_ALU_o1_1_1_2_1_bus_cntrl_reg <= conv_std_logic_vector(0, socket_ALU_o1_1_1_2_1_bus_cntrl_reg'length);
        elsif (squash_B5_1_1_2_1_1_1_1_1 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1_1_1_1_1(4 downto 3))) = 0) then
          fu_VALU_int0_load_reg <= '1';
          fu_VALU_opc_reg <= dst_B5_1_1_2_1_1_1_1_1(2 downto 0);
          socket_ALU_o1_1_1_2_1_bus_cntrl_reg <= conv_std_logic_vector(2, socket_ALU_o1_1_1_2_1_bus_cntrl_reg'length);
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1_1 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1_1_1_1_1_1_1_1(4 downto 3))) = 0) then
          fu_VALU_int0_load_reg <= '1';
          fu_VALU_opc_reg <= dst_B5_1_1_2_1_1_1_1_1_1_1_1(2 downto 0);
          socket_ALU_o1_1_1_2_1_bus_cntrl_reg <= conv_std_logic_vector(1, socket_ALU_o1_1_1_2_1_bus_cntrl_reg'length);
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1_3 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1_1_1_1_1_1_1_3(5 downto 3))) = 4) then
          fu_VALU_int0_load_reg <= '1';
          fu_VALU_opc_reg <= dst_B5_1_1_2_1_1_1_1_1_1_1_3(2 downto 0);
          socket_ALU_o1_1_1_2_1_bus_cntrl_reg <= conv_std_logic_vector(3, socket_ALU_o1_1_1_2_1_bus_cntrl_reg'length);
        else
          fu_VALU_int0_load_reg <= '0';
        end if;
        if (squash_B5_1_1_2_1_1 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1_1(2 downto 1))) = 2) then
          fu_VALU_in2_load_reg <= '1';
          socket_ALU_o1_1_1_2_1_1_bus_cntrl_reg <= conv_std_logic_vector(0, socket_ALU_o1_1_1_2_1_1_bus_cntrl_reg'length);
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1_2 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1_1_1_1_1_1_1_2(1 downto 0))) = 0) then
          fu_VALU_in2_load_reg <= '1';
          socket_ALU_o1_1_1_2_1_1_bus_cntrl_reg <= conv_std_logic_vector(1, socket_ALU_o1_1_1_2_1_1_bus_cntrl_reg'length);
        else
          fu_VALU_in2_load_reg <= '0';
        end if;
        if (squash_B5_1_1_2_1 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1(5 downto 1))) = 24) then
          fu_VALU_in1_load_reg <= '1';
          socket_ALU_o1_1_1_2_1_2_bus_cntrl_reg <= conv_std_logic_vector(0, socket_ALU_o1_1_1_2_1_2_bus_cntrl_reg'length);
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1_1 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1_1_1_1_1_1_1_1(4 downto 2))) = 5) then
          fu_VALU_in1_load_reg <= '1';
          socket_ALU_o1_1_1_2_1_2_bus_cntrl_reg <= conv_std_logic_vector(1, socket_ALU_o1_1_1_2_1_2_bus_cntrl_reg'length);
        else
          fu_VALU_in1_load_reg <= '0';
        end if;
        if (squash_B2 = '0' and conv_integer(unsigned(dst_B2(5 downto 0))) = 30) then
          fu_VBCAST_in1t_load_reg <= '1';
        else
          fu_VBCAST_in1t_load_reg <= '0';
        end if;
        if (squash_B5_1_1_2_1_1 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1_1(2 downto 2))) = 0) then
          fu_VSHUFFLE_in1t_load_reg <= '1';
          fu_VSHUFFLE_opc_reg <= dst_B5_1_1_2_1_1(1 downto 0);
          socket_ALU_i1_1_1_1_1_1_1_bus_cntrl_reg <= conv_std_logic_vector(1, socket_ALU_i1_1_1_1_1_1_1_bus_cntrl_reg'length);
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1_1 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1_1_1_1_1_1_1_1(4 downto 2))) = 4) then
          fu_VSHUFFLE_in1t_load_reg <= '1';
          fu_VSHUFFLE_opc_reg <= dst_B5_1_1_2_1_1_1_1_1_1_1_1(1 downto 0);
          socket_ALU_i1_1_1_1_1_1_1_bus_cntrl_reg <= conv_std_logic_vector(0, socket_ALU_i1_1_1_1_1_1_1_bus_cntrl_reg'length);
        else
          fu_VSHUFFLE_in1t_load_reg <= '0';
        end if;
        if (squash_B5_1_1_2_1_1_1_1_1 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1_1_1_1_1(4 downto 3))) = 2) then
          fu_VSHUFFLE_in2_load_reg <= '1';
          socket_ALU_i1_1_1_1_2_1_bus_cntrl_reg <= conv_std_logic_vector(0, socket_ALU_i1_1_1_1_2_1_bus_cntrl_reg'length);
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1_1 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1_1_1_1_1_1_1_1(4 downto 2))) = 6) then
          fu_VSHUFFLE_in2_load_reg <= '1';
          socket_ALU_i1_1_1_1_2_1_bus_cntrl_reg <= conv_std_logic_vector(1, socket_ALU_i1_1_1_1_2_1_bus_cntrl_reg'length);
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1_2 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1_1_1_1_1_1_1_2(1 downto 0))) = 1) then
          fu_VSHUFFLE_in2_load_reg <= '1';
          socket_ALU_i1_1_1_1_2_1_bus_cntrl_reg <= conv_std_logic_vector(2, socket_ALU_i1_1_1_1_2_1_bus_cntrl_reg'length);
        else
          fu_VSHUFFLE_in2_load_reg <= '0';
        end if;
        if (squash_B5_1_1_2_1_1_1_1_1_1_1_2 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1_1_1_1_1_1_1_2(1 downto 0))) = 2) then
          fu_VEXTRACT_in1t_load_reg <= '1';
        else
          fu_VEXTRACT_in1t_load_reg <= '0';
        end if;
        if (squash_B2 = '0' and conv_integer(unsigned(dst_B2(5 downto 0))) = 31) then
          fu_VEXTRACT_in2_load_reg <= '1';
        else
          fu_VEXTRACT_in2_load_reg <= '0';
        end if;
        if (squash_B2 = '0' and conv_integer(unsigned(dst_B2(5 downto 4))) = 0) then
          fu_ALU_1_in1t_load_reg <= '1';
          fu_ALU_1_opc_reg <= dst_B2(3 downto 0);
          socket_ALU_i2_2_bus_cntrl_reg <= conv_std_logic_vector(0, socket_ALU_i2_2_bus_cntrl_reg'length);
        elsif (squash_B5_3 = '0' and conv_integer(unsigned(dst_B5_3(5 downto 4))) = 1) then
          fu_ALU_1_in1t_load_reg <= '1';
          fu_ALU_1_opc_reg <= dst_B5_3(3 downto 0);
          socket_ALU_i2_2_bus_cntrl_reg <= conv_std_logic_vector(1, socket_ALU_i2_2_bus_cntrl_reg'length);
        else
          fu_ALU_1_in1t_load_reg <= '0';
        end if;
        if (squash_B1 = '0' and conv_integer(unsigned(dst_B1(6 downto 0))) = 63) then
          fu_ALU_1_in2_load_reg <= '1';
          socket_ALU_i2_1_bus_cntrl_reg <= conv_std_logic_vector(0, socket_ALU_i2_1_bus_cntrl_reg'length);
        elsif (squash_B5_3 = '0' and conv_integer(unsigned(dst_B5_3(5 downto 2))) = 10) then
          fu_ALU_1_in2_load_reg <= '1';
          socket_ALU_i2_1_bus_cntrl_reg <= conv_std_logic_vector(1, socket_ALU_i2_1_bus_cntrl_reg'length);
        else
          fu_ALU_1_in2_load_reg <= '0';
        end if;
        if (squash_B2 = '0' and conv_integer(unsigned(dst_B2(5 downto 3))) = 2) then
          fu_PARAM_LSU_in1t_load_reg <= '1';
          fu_PARAM_LSU_opc_reg <= dst_B2(2 downto 0);
        else
          fu_PARAM_LSU_in1t_load_reg <= '0';
        end if;
        if (squash_B1 = '0' and conv_integer(unsigned(dst_B1(6 downto 0))) = 62) then
          fu_PARAM_LSU_in2_load_reg <= '1';
        else
          fu_PARAM_LSU_in2_load_reg <= '0';
        end if;
        if (squash_B1 = '0' and conv_integer(unsigned(dst_B1(6 downto 3))) = 6) then
          fu_DATA_LSU_in1t_load_reg <= '1';
          fu_DATA_LSU_opc_reg <= dst_B1(2 downto 0);
          socket_ALU_i1_1_1_1_1_bus_cntrl_reg <= conv_std_logic_vector(0, socket_ALU_i1_1_1_1_1_bus_cntrl_reg'length);
        elsif (squash_B5_3 = '0' and conv_integer(unsigned(dst_B5_3(5 downto 3))) = 4) then
          fu_DATA_LSU_in1t_load_reg <= '1';
          fu_DATA_LSU_opc_reg <= dst_B5_3(2 downto 0);
          socket_ALU_i1_1_1_1_1_bus_cntrl_reg <= conv_std_logic_vector(1, socket_ALU_i1_1_1_1_1_bus_cntrl_reg'length);
        else
          fu_DATA_LSU_in1t_load_reg <= '0';
        end if;
        if (squash_B1 = '0' and conv_integer(unsigned(dst_B1(6 downto 0))) = 64) then
          fu_DATA_LSU_in2_load_reg <= '1';
          socket_ALU_i1_1_1_1_2_bus_cntrl_reg <= conv_std_logic_vector(1, socket_ALU_i1_1_1_1_2_bus_cntrl_reg'length);
        elsif (squash_B2 = '0' and conv_integer(unsigned(dst_B2(5 downto 0))) = 32) then
          fu_DATA_LSU_in2_load_reg <= '1';
          socket_ALU_i1_1_1_1_2_bus_cntrl_reg <= conv_std_logic_vector(0, socket_ALU_i1_1_1_1_2_bus_cntrl_reg'length);
        else
          fu_DATA_LSU_in2_load_reg <= '0';
        end if;
        if (squash_B1 = '0' and conv_integer(unsigned(dst_B1(6 downto 0))) = 65) then
          fu_Streamin1_t1_load_reg <= '1';
          socket_StreamSock1_bus_cntrl_reg <= conv_std_logic_vector(0, socket_StreamSock1_bus_cntrl_reg'length);
        elsif (squash_B2 = '0' and conv_integer(unsigned(dst_B2(5 downto 0))) = 33) then
          fu_Streamin1_t1_load_reg <= '1';
          socket_StreamSock1_bus_cntrl_reg <= conv_std_logic_vector(1, socket_StreamSock1_bus_cntrl_reg'length);
        elsif (squash_B5_3 = '0' and conv_integer(unsigned(dst_B5_3(5 downto 2))) = 11) then
          fu_Streamin1_t1_load_reg <= '1';
          socket_StreamSock1_bus_cntrl_reg <= conv_std_logic_vector(2, socket_StreamSock1_bus_cntrl_reg'length);
        else
          fu_Streamin1_t1_load_reg <= '0';
        end if;
        if (squash_B1 = '0' and conv_integer(unsigned(dst_B1(6 downto 0))) = 66) then
          fu_Streamin2_t1_load_reg <= '1';
          socket_StreamSock2_bus_cntrl_reg <= conv_std_logic_vector(1, socket_StreamSock2_bus_cntrl_reg'length);
        elsif (squash_B2 = '0' and conv_integer(unsigned(dst_B2(5 downto 0))) = 34) then
          fu_Streamin2_t1_load_reg <= '1';
          socket_StreamSock2_bus_cntrl_reg <= conv_std_logic_vector(0, socket_StreamSock2_bus_cntrl_reg'length);
        elsif (squash_B5_3 = '0' and conv_integer(unsigned(dst_B5_3(5 downto 2))) = 12) then
          fu_Streamin2_t1_load_reg <= '1';
          socket_StreamSock2_bus_cntrl_reg <= conv_std_logic_vector(2, socket_StreamSock2_bus_cntrl_reg'length);
        else
          fu_Streamin2_t1_load_reg <= '0';
        end if;
        if (squash_B5_1_1_2_1 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1(5 downto 1))) = 25) then
          fu_Streamout_t1_load_reg <= '1';
          socket_StreamSockV3_bus_cntrl_reg <= conv_std_logic_vector(0, socket_StreamSockV3_bus_cntrl_reg'length);
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1_1_1_1_1_1_1(1 downto 0))) = 0) then
          fu_Streamout_t1_load_reg <= '1';
          socket_StreamSockV3_bus_cntrl_reg <= conv_std_logic_vector(1, socket_StreamSockV3_bus_cntrl_reg'length);
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1_3 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1_1_1_1_1_1_1_3(5 downto 2))) = 12) then
          fu_Streamout_t1_load_reg <= '1';
          socket_StreamSockV3_bus_cntrl_reg <= conv_std_logic_vector(2, socket_StreamSockV3_bus_cntrl_reg'length);
        else
          fu_Streamout_t1_load_reg <= '0';
        end if;
        if (squash_B5_1_1_2_1 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1(5 downto 1))) = 26) then
          fu_Streamout2_t1_load_reg <= '1';
          socket_StreamSockV3_1_bus_cntrl_reg <= conv_std_logic_vector(0, socket_StreamSockV3_1_bus_cntrl_reg'length);
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1_1_1_1_1_1_1(1 downto 0))) = 1) then
          fu_Streamout2_t1_load_reg <= '1';
          socket_StreamSockV3_1_bus_cntrl_reg <= conv_std_logic_vector(1, socket_StreamSockV3_1_bus_cntrl_reg'length);
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1_3 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1_1_1_1_1_1_1_3(5 downto 2))) = 13) then
          fu_Streamout2_t1_load_reg <= '1';
          socket_StreamSockV3_1_bus_cntrl_reg <= conv_std_logic_vector(2, socket_StreamSockV3_1_bus_cntrl_reg'length);
        else
          fu_Streamout2_t1_load_reg <= '0';
        end if;
        if (squash_B1 = '0' and conv_integer(unsigned(dst_B1(6 downto 0))) = 67) then
          fu_Streamin3_t1_load_reg <= '1';
          socket_StreamSock2_1_bus_cntrl_reg <= conv_std_logic_vector(1, socket_StreamSock2_1_bus_cntrl_reg'length);
        elsif (squash_B2 = '0' and conv_integer(unsigned(dst_B2(5 downto 0))) = 35) then
          fu_Streamin3_t1_load_reg <= '1';
          socket_StreamSock2_1_bus_cntrl_reg <= conv_std_logic_vector(0, socket_StreamSock2_1_bus_cntrl_reg'length);
        elsif (squash_B5_3 = '0' and conv_integer(unsigned(dst_B5_3(5 downto 2))) = 13) then
          fu_Streamin3_t1_load_reg <= '1';
          socket_StreamSock2_1_bus_cntrl_reg <= conv_std_logic_vector(2, socket_StreamSock2_1_bus_cntrl_reg'length);
        else
          fu_Streamin3_t1_load_reg <= '0';
        end if;
        if (squash_B5_1_1_2_1 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1(5 downto 1))) = 27) then
          fu_Streamout3_t1_load_reg <= '1';
          socket_StreamSockV3_2_bus_cntrl_reg <= conv_std_logic_vector(0, socket_StreamSockV3_2_bus_cntrl_reg'length);
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1_1_1_1_1_1_1(1 downto 0))) = 2) then
          fu_Streamout3_t1_load_reg <= '1';
          socket_StreamSockV3_2_bus_cntrl_reg <= conv_std_logic_vector(1, socket_StreamSockV3_2_bus_cntrl_reg'length);
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1_3 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1_1_1_1_1_1_1_3(5 downto 2))) = 14) then
          fu_Streamout3_t1_load_reg <= '1';
          socket_StreamSockV3_2_bus_cntrl_reg <= conv_std_logic_vector(2, socket_StreamSockV3_2_bus_cntrl_reg'length);
        else
          fu_Streamout3_t1_load_reg <= '0';
        end if;
        if (squash_B5_1_1_2_1 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1(5 downto 3))) = 5) then
          fu_VALU_1_int0_load_reg <= '1';
          fu_VALU_1_opc_reg <= dst_B5_1_1_2_1(2 downto 0);
          socket_ALU_o1_1_1_2_1_3_bus_cntrl_reg <= conv_std_logic_vector(0, socket_ALU_o1_1_1_2_1_3_bus_cntrl_reg'length);
        elsif (squash_B5_1_1_2_1_1_1_1_1 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1_1_1_1_1(4 downto 3))) = 1) then
          fu_VALU_1_int0_load_reg <= '1';
          fu_VALU_1_opc_reg <= dst_B5_1_1_2_1_1_1_1_1(2 downto 0);
          socket_ALU_o1_1_1_2_1_3_bus_cntrl_reg <= conv_std_logic_vector(2, socket_ALU_o1_1_1_2_1_3_bus_cntrl_reg'length);
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1_1 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1_1_1_1_1_1_1_1(4 downto 3))) = 1) then
          fu_VALU_1_int0_load_reg <= '1';
          fu_VALU_1_opc_reg <= dst_B5_1_1_2_1_1_1_1_1_1_1_1(2 downto 0);
          socket_ALU_o1_1_1_2_1_3_bus_cntrl_reg <= conv_std_logic_vector(1, socket_ALU_o1_1_1_2_1_3_bus_cntrl_reg'length);
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1_3 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1_1_1_1_1_1_1_3(5 downto 3))) = 5) then
          fu_VALU_1_int0_load_reg <= '1';
          fu_VALU_1_opc_reg <= dst_B5_1_1_2_1_1_1_1_1_1_1_3(2 downto 0);
          socket_ALU_o1_1_1_2_1_3_bus_cntrl_reg <= conv_std_logic_vector(3, socket_ALU_o1_1_1_2_1_3_bus_cntrl_reg'length);
        else
          fu_VALU_1_int0_load_reg <= '0';
        end if;
        if (squash_B5_1_1_2_1_1 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1_1(2 downto 1))) = 3) then
          fu_VALU_1_in2_load_reg <= '1';
          socket_ALU_o1_1_1_2_1_1_1_bus_cntrl_reg <= conv_std_logic_vector(0, socket_ALU_o1_1_1_2_1_1_1_bus_cntrl_reg'length);
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1_2 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1_1_1_1_1_1_1_2(1 downto 0))) = 3) then
          fu_VALU_1_in2_load_reg <= '1';
          socket_ALU_o1_1_1_2_1_1_1_bus_cntrl_reg <= conv_std_logic_vector(1, socket_ALU_o1_1_1_2_1_1_1_bus_cntrl_reg'length);
        else
          fu_VALU_1_in2_load_reg <= '0';
        end if;
        if (squash_B5_1_1_2_1 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1(5 downto 1))) = 28) then
          fu_VALU_1_in1_load_reg <= '1';
          socket_ALU_o1_1_1_2_1_2_1_bus_cntrl_reg <= conv_std_logic_vector(0, socket_ALU_o1_1_1_2_1_2_1_bus_cntrl_reg'length);
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1_1 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1_1_1_1_1_1_1_1(4 downto 2))) = 7) then
          fu_VALU_1_in1_load_reg <= '1';
          socket_ALU_o1_1_1_2_1_2_1_bus_cntrl_reg <= conv_std_logic_vector(1, socket_ALU_o1_1_1_2_1_2_1_bus_cntrl_reg'length);
        else
          fu_VALU_1_in1_load_reg <= '0';
        end if;
        if (squash_B1 = '0' and conv_integer(unsigned(dst_B1(6 downto 1))) = 29) then
          fu_gcu_pc_load_reg <= '1';
          fu_gcu_opc_reg <= dst_B1(0 downto 0);
          socket_gcu_i1_bus_cntrl_reg <= conv_std_logic_vector(0, socket_gcu_i1_bus_cntrl_reg'length);
        elsif (squash_B2 = '0' and conv_integer(unsigned(dst_B2(5 downto 1))) = 13) then
          fu_gcu_pc_load_reg <= '1';
          fu_gcu_opc_reg <= dst_B2(0 downto 0);
          socket_gcu_i1_bus_cntrl_reg <= conv_std_logic_vector(1, socket_gcu_i1_bus_cntrl_reg'length);
        else
          fu_gcu_pc_load_reg <= '0';
        end if;
        if (squash_B1 = '0' and conv_integer(unsigned(dst_B1(6 downto 0))) = 60) then
          fu_gcu_ra_load_reg <= '1';
          socket_gcu_i2_bus_cntrl_reg <= conv_std_logic_vector(0, socket_gcu_i2_bus_cntrl_reg'length);
        elsif (squash_B2 = '0' and conv_integer(unsigned(dst_B2(5 downto 0))) = 28) then
          fu_gcu_ra_load_reg <= '1';
          socket_gcu_i2_bus_cntrl_reg <= conv_std_logic_vector(1, socket_gcu_i2_bus_cntrl_reg'length);
        else
          fu_gcu_ra_load_reg <= '0';
        end if;
        -- control signals for RF inputs
        if (squash_B1 = '0' and conv_integer(unsigned(dst_B1(6 downto 1))) = 28 and true) then
          rf_BOOL_wr_load_reg <= '1';
          rf_BOOL_wr_opc_reg <= dst_B1(0 downto 0);
          socket_bool_i1_bus_cntrl_reg <= conv_std_logic_vector(0, socket_bool_i1_bus_cntrl_reg'length);
        elsif (squash_B2 = '0' and conv_integer(unsigned(dst_B2(5 downto 1))) = 12 and true) then
          rf_BOOL_wr_load_reg <= '1';
          rf_BOOL_wr_opc_reg <= dst_B2(0 downto 0);
          socket_bool_i1_bus_cntrl_reg <= conv_std_logic_vector(1, socket_bool_i1_bus_cntrl_reg'length);
        else
          rf_BOOL_wr_load_reg <= '0';
        end if;
        if (squash_B1 = '0' and conv_integer(unsigned(dst_B1(6 downto 5))) = 0 and true) then
          rf_RF_32_0_W1_32_load_reg <= '1';
          rf_RF_32_0_W1_32_opc_reg <= dst_B1(4 downto 0);
        else
          rf_RF_32_0_W1_32_load_reg <= '0';
        end if;
        if (squash_B5_1_1_2_1 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1(5 downto 5))) = 0 and true) then
          rf_RF_256_0_R1_32_load_reg <= '1';
          rf_RF_256_0_R1_32_opc_reg <= dst_B5_1_1_2_1(4 downto 0);
          socket_W1_32_1_bus_cntrl_reg <= conv_std_logic_vector(1, socket_W1_32_1_bus_cntrl_reg'length);
        elsif (squash_B5_1_1_2_1_1_1_1_1_1_1_3 = '0' and conv_integer(unsigned(dst_B5_1_1_2_1_1_1_1_1_1_1_3(5 downto 5))) = 0 and true) then
          rf_RF_256_0_R1_32_load_reg <= '1';
          rf_RF_256_0_R1_32_opc_reg <= dst_B5_1_1_2_1_1_1_1_1_1_1_3(4 downto 0);
          socket_W1_32_1_bus_cntrl_reg <= conv_std_logic_vector(0, socket_W1_32_1_bus_cntrl_reg'length);
        else
          rf_RF_256_0_R1_32_load_reg <= '0';
        end if;
      end if;
    end if;
  end process;

  lock_reg_proc : process (clk, rstx)
  begin
    if (rstx = '0') then
      -- Locked during active reset      post_decode_merged_glock_r <= '1';
    elsif (clk'event and clk = '1') then
      post_decode_merged_glock_r <= post_decode_merged_glock;
    end if;
  end process lock_reg_proc;

  lock_r <= merged_glock_req;
  merged_glock_req <= lock_req(0) or lock_req(1) or lock_req(2) or lock_req(3) or lock_req(4) or lock_req(5) or lock_req(6) or lock_req(7) or lock_req(8) or lock_req(9) or lock_req(10) or lock_req(11) or lock_req(12) or lock_req(13) or lock_req(14) or lock_req(15);
  pre_decode_merged_glock <= lock or merged_glock_req;
  post_decode_merged_glock <= pre_decode_merged_glock or decode_fill_lock_reg;
  locked <= post_decode_merged_glock_r;
  glock(0) <= post_decode_merged_glock; -- to ALU_0
  glock(1) <= post_decode_merged_glock; -- to VALU
  glock(2) <= post_decode_merged_glock; -- to VBCAST
  glock(3) <= post_decode_merged_glock; -- to VSHUFFLE
  glock(4) <= post_decode_merged_glock; -- to VEXTRACT
  glock(5) <= post_decode_merged_glock; -- to ALU_1
  glock(6) <= post_decode_merged_glock; -- to PARAM_LSU
  glock(7) <= post_decode_merged_glock; -- to DATA_LSU
  glock(8) <= post_decode_merged_glock; -- to Streamin1
  glock(9) <= post_decode_merged_glock; -- to Streamin2
  glock(10) <= post_decode_merged_glock; -- to Streamout
  glock(11) <= post_decode_merged_glock; -- to Streamout2
  glock(12) <= post_decode_merged_glock; -- to Streamin3
  glock(13) <= post_decode_merged_glock; -- to Streamout3
  glock(14) <= post_decode_merged_glock; -- to VALU_1
  glock(15) <= post_decode_merged_glock; -- to BOOL
  glock(16) <= post_decode_merged_glock; -- to RF_32_0
  glock(17) <= post_decode_merged_glock; -- to RF_256_0
  glock(18) <= post_decode_merged_glock; -- to IU_1x32
  glock(19) <= post_decode_merged_glock;

  decode_pipeline_fill_lock: process (clk, rstx)
  begin
    if rstx = '0' then
      decode_fill_lock_reg <= '1';
    elsif clk'event and clk = '1' then
      if lock = '0' then
        decode_fill_lock_reg <= '0';
      end if;
    end if;
  end process decode_pipeline_fill_lock;

end rtl_andor;
