library IEEE;
use IEEE.std_logic_1164.all;

package streaming_simd_tta_gcu_opcodes is
  constant IFE_CALL : natural := 0;
  constant IFE_JUMP : natural := 1;
end streaming_simd_tta_gcu_opcodes;
