/**
 * OSAL behavior definition file.
 */

#include "OSAL.hh"

#include <fstream>
#include "Application.hh"

union char_int {
    char chars[4];
    int32_t num;
};



//////////////////////////////////////////////////////////////////////////////
// INPUT_STREAM - State definition for the STREAM_IN.
//
// Opens a file simulating the input stream. Default filename is tta_stream.in,
// and can be changed with the environment variable TTASIM_STREAM_IN_FILE.
//////////////////////////////////////////////////////////////////////////////

DEFINE_STATE(INPUT_STREAM)
    std::ifstream inputFile;

INIT_STATE(INPUT_STREAM)
    const char* fileNameFromEnv = getenv("TTASIM_STREAM_IN_FILE");
    std::string fileName = "";
    if (fileNameFromEnv == NULL) {
        fileName = context.functionUnitName() + ".in";
    } else {
        fileName = fileNameFromEnv;
    }
    inputFile.open(fileName.c_str(), std::ios_base::binary);
    if (!inputFile.is_open()) {
        OUTPUT_STREAM 
            << "Cannot open input stream file " 
            << fileName << std::endl;
    }
END_INIT_STATE;

FINALIZE_STATE(INPUT_STREAM)
    inputFile.close();
END_FINALIZE_STATE;

END_DEFINE_STATE

//////////////////////////////////////////////////////////////////////////////
// STREAM_IN - Reads a sample from the default input stream.
//
// @todo: Support for other sample sizes than 256.
//////////////////////////////////////////////////////////////////////////////

OPERATION_WITH_STATE(STREAM_IN_256, INPUT_STREAM)

TRIGGER
    if (BWIDTH(2) != 256) {
        Application::logStream() 
            << "STREAM_IN works with 256b only at the moment." 
            << std::endl;
    }
    if (!STATE.inputFile.is_open()) {
        IO(2) = 0;
        return true;
    }
    
    union char_int input;
    for (UIntWord i = 0; i < 8; ++i){
        STATE.inputFile.read(input.chars, 4);
        SET_SUBWORD32(2,i,static_cast<Word>(input.num));
    }
END_TRIGGER;

END_OPERATION_WITH_STATE(STREAM_IN_256)

//////////////////////////////////////////////////////////////////////////////
// OUTPUT_STREAM - State definition for the STREAM_OUT.
//
// Opens a file simulating the output stream. Default filename is 
// tta_stream.out, and can be changed with the environment variable 
// TTASIM_STREAM_IN_FILE.
//////////////////////////////////////////////////////////////////////////////

DEFINE_STATE(OUTPUT_STREAM)
    std::ofstream outputFile;

INIT_STATE(OUTPUT_STREAM)
 
    const char* fileNameFromEnv = getenv("TTASIM_STREAM_OUT_FILE");
    std::string fileName = "";
    if (fileNameFromEnv == NULL) {
        fileName = context.functionUnitName() + ".out";
    } else {
        fileName = fileNameFromEnv;
    }
    outputFile.open(
        fileName.c_str(), 
        std::ios_base::out | std::ios_base::trunc | std::ios_base::binary);
    if (!outputFile.is_open()) {
        OUTPUT_STREAM 
            << "Cannot open output file!" 
            << fileName << std::endl;
    }
END_INIT_STATE;

FINALIZE_STATE(OUTPUT_STREAM)
    outputFile.close();
END_FINALIZE_STATE;

END_DEFINE_STATE

//////////////////////////////////////////////////////////////////////////////
// STREAM_OUT - Writes a sample to the default output stream.
//
// @todo: Support for other sample sizes than 8.
//////////////////////////////////////////////////////////////////////////////

OPERATION_WITH_STATE(STREAM_OUT_256, OUTPUT_STREAM)

TRIGGER

    if (BWIDTH(1) != 256) 
        Application::logStream() 
            << "STREAM_OUT works with 256b only at the moment." 
            << std::endl;
    for (UIntWord i = 0; i < 8; ++i){
        union char_int data;
        data.num = static_cast<Word>(SUBWORD32(1,i));
        STATE.outputFile.write(data.chars, 4);
    }
    STATE.outputFile << std::flush;

    if (STATE.outputFile.fail()) {
        OUTPUT_STREAM << "error while writing the output file" << std::endl;
    }
END_TRIGGER;
END_OPERATION_WITH_STATE(STREAM_OUT_256)

