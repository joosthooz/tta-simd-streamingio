#ifndef _VEC3I3O_H_
#define _VEC3I3O_H_

#include <inttypes.h>
#include <tce_vector.h>

int vec3i3o(int8 (*pop1)(void), int8 (*pop2)(void), int8 (*pop3)(void), void (*push1)(int8), void (*push2)(int8), void (*push3)(int8));

#endif
